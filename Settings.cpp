#include "Settings.h"

#include "Globals.h"

Settings& Settings::instance()
{
    static Settings s_settings(QSettings::IniFormat, QSettings::UserScope, APP_NAME, APP_NAME);
    return s_settings;
}

QVariant Settings::getValue(const QString& strKey,
                            const QVariant& varDefaultValue /* = QVariant()*/)
{
    const QVariant varValue = QSettings::value(strKey);
    if (varValue.isNull() && varDefaultValue.isValid())
    {
        setValue(strKey, varDefaultValue);
        return varDefaultValue;
    }
    return varValue;
}

QVariant Settings::value(const QString& key, const QVariant& defaultValue /* = QVariant()*/) const
{
    return QSettings::value(key, defaultValue);
}
