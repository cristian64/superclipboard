#ifndef UTILS_H
#define UTILS_H

#include <QtCore/QString>

#define Q_CONNECT(a, b, c, d)                        \
    do                                               \
    {                                                \
        const bool bConnected = connect(a, b, c, d); \
        Q_ASSERT(bConnected);                        \
    } while (false)
#define Q_DISCONNECT(a, b, c, d)                           \
    do                                                     \
    {                                                      \
        const bool bDisconnected = disconnect(a, b, c, d); \
        Q_ASSERT(bDisconnected);                           \
    } while (false)

namespace Utils
{
QString cleanPath(const QString& strPath);
QString getHostName();
QString convertToHumanReadableSize(const quint64 nBytes);
bool isValidUrl(const QString& strUr);
}  // namespace Utils

#endif  // UTILS_H
