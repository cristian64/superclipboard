#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QtWidgets/QDialog>

#include "MainWindow.h"

namespace Ui
{
class SettingsDialog;
}

class SettingsDialog final : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(MainWindow* parent = 0);
    ~SettingsDialog();

    Q_DISABLE_COPY(SettingsDialog)

protected slots:
    void on_spinBoxMaxHistoryCount_valueChanged(int nValue);
    void on_checkBoxConfirmDeletion_toggled(bool bChecked);
    void on_checkBoxShowPopups_toggled(bool bChecked);
    void on_checkBoxShowNotifications_toggled(bool bChecked);
    void on_checkBoxShowSystemTrayIcon_toggled(bool bChecked);
    void on_pushButtonClose_clicked();
    void on_pushButtonBrowse_clicked();
    void on_lineEditDataLocation_editingFinished();

private:
    Ui::SettingsDialog* ui;
    MainWindow* m_pMainWindow;
};

#endif  // SETTINGSDIALOG_H
