#ifndef CLIPBOARDMANAGER_H
#define CLIPBOARDMANAGER_H

#include <memory>

#include <QtCore/QDateTime>
#include <QtCore/QMimeData>
#include <QtGui/QClipboard>

class ClipboardManager final : public QObject
{
    Q_OBJECT

public:
    static ClipboardManager& instance();

    ClipboardManager();
    ~ClipboardManager();

    bool isEnabled() const;
    void setEnabled(const bool bEnabled);

    void setMimeData(const QMimeData& mimeData);

    void emitSlotChanged(const quint8 nSlot);

    static void readAndStoreClipboardContent(
        const QString& strFilePath,
        const QDateTime& dtTime = QDateTime::currentDateTimeUtc());

signals:

    void sig_clipboardChanged(const QMimeData& MimeData);
    void sig_slotChanged(const QMimeData& pMimeData);
    void sig_slotChanged(const QMimeData& pMimeData, const quint8 nSlot);
    void sig_slotRequested(const quint8 nSlot);
    void sig_toggleClipboard();
    void sig_scrollUp();
    void sig_scrollDown();

private slots:

    void slt_clipboardChanged(QClipboard::Mode mode);
    void slt_slotActivated(const quint8 nSlot, const qint32 eActivationMode);

private:
    Q_DISABLE_COPY(ClipboardManager)

    class PrivateData;
    std::unique_ptr<PrivateData> d;
};

#endif  // CLIPBOARDMANAGER_H
