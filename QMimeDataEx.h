#ifndef QMIMEDATAEX_H
#define QMIMEDATAEX_H

#include <QtCore/QDataStream>
#include <QtCore/QMimeData>

QDataStream& operator<<(QDataStream& stream, const QMimeData& mimeData);
QDataStream& operator>>(QDataStream& stream, QMimeData& mimeData);
bool operator==(const QMimeData& mimeData, const QMimeData& other);

namespace QMimeDataEx
{
QMimeData* cloneMimeData(const QMimeData& mimeData);
void copyMimeData(const QMimeData& mimeDataSource, QMimeData& mimeDataDestination);
bool hasData(const QMimeData& mimeData);
quint64 calculateSize(const QMimeData& mimeData);
bool getImage(const QMimeData& mimeData, QImage& image);
}  // namespace QMimeDataEx

#endif  // QMIMEDATAEX_H
