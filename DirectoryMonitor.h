#ifndef DIRECTORYMONITOR_H
#define DIRECTORYMONITOR_H

#include <memory>

#include <QtCore/QFileSystemWatcher>
#include <QtCore/QObject>
#include <QtCore/QStringList>
#include <QtCore/QVector>

class DirectoryMonitor final : public QObject
{
    Q_OBJECT

public:
    static DirectoryMonitor& instance();

    DirectoryMonitor(const QString& strDirPath);
    ~DirectoryMonitor();

    QVector<QString> getFilePaths(const QStringList& strlstNameFilters = QStringList()) const;

signals:
    void sig_fileRemoved(const QString& strFilePath);
    void sig_fileAdded(const QString& strFilePath);

private:
    Q_DISABLE_COPY(DirectoryMonitor)

    class PrivateData;
    std::unique_ptr<PrivateData> d;
};

#endif  // DIRECTORYMONITOR_H
