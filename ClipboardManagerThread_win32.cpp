// clang-format off
#include "ClipboardManagerThread.h"
// clang-format on

#include <windows.h>

#include <mutex>
#include <unordered_set>

#include <QtCore/QCoreApplication>

#define KEY_ALT 164
#define KEY_SUPER 91
#define KEY_1 49
#define KEY_9 57
#define KEY_F1 112
#define KEY_F9 120
#define KEY_COUNT 9  // Handling from [1] to [9] (and from [F1] to [F9]).
#define KEY_Q 81     // For toggling the last two clipboards.

// Some helper macros to encode a pair of values into a QEvent::Type value.
#define MAKE_CUSTOM_TYPE(slot, mode) (QEvent::Type(QEvent::User + slot + mode * 1000))
#define IS_CUSTOM_TYPE(type) (QEvent::User <= type && type <= QEvent::User + 1000 + KEY_COUNT + 1)
#define COMPOUND_MODE_FROM_TYPE(type)                                           \
    (type < QEvent::User + 1000 ? ClipboardManagerThread::ACTIVATION_MODE_STORE \
                                : ClipboardManagerThread::ACTIVATION_MODE_RETRIEVE)
#define COMPOUND_SLOT_FROM_TYPE(type) (type - QEvent::User - COMPOUND_MODE_FROM_TYPE(type) * 1000);
#define MAKE_TOGGLE_TYPE() (QEvent::Type(QEvent::User + 10000))
#define IS_TOGGLE_TYPE(type) (type == QEvent::Type(QEvent::User + 10000))
#define MAKE_WHEEL_TYPE(delta) (QEvent::Type(QEvent::User + 20000 + (delta > 0 ? 1 : 0)))
#define IS_WHEEL_TYPE(type) \
    (type == QEvent::Type(QEvent::User + 20000) || type == QEvent::Type(QEvent::User + 20001))
#define WHEEL_DIRECTION_FROM_TYPE(type) (type == QEvent::Type(QEvent::User + 20000))

namespace
{
struct KeyboardState
{
    KeyboardState()
        : m_hKeyboardHook(nullptr),
          m_hMouseHook(nullptr),
          m_bSuperPressed(false),
          m_bAltPressed(false),
          m_nNumberPressed(-1),
          m_nFNumberPressed(-1),
          m_bQPressed(false)
    {
    }

    HHOOK m_hKeyboardHook;
    HHOOK m_hMouseHook;
    bool m_bSuperPressed;
    bool m_bAltPressed;
    int m_nNumberPressed;
    int m_nFNumberPressed;
    bool m_bQPressed;
};

std::mutex g_mutex;
std::unordered_set<ClipboardManagerThread*> g_lstThread;
KeyboardState g_keyboardState;

LRESULT CALLBACK keyboardProcedure(int nCode, WPARAM wParam, LPARAM lParam)
{
    if (nCode != HC_ACTION || (wParam != WM_KEYDOWN && wParam != WM_KEYUP &&
                               wParam != WM_SYSKEYDOWN && wParam != WM_SYSKEYUP))
    {
        return false;
    }

    // Handle the common part under a lock.
    std::lock_guard lock(g_mutex);
    KeyboardState& keyboardState = g_keyboardState;

    const KBDLLHOOKSTRUCT* const pHookStruct = (KBDLLHOOKSTRUCT*)lParam;
    if (wParam == WM_KEYDOWN || wParam == WM_SYSKEYDOWN)
    {
        // Handle the state change of the keys.
        bool bStateChanged = false;
        if (pHookStruct->vkCode == KEY_ALT && !keyboardState.m_bAltPressed)
        {
            keyboardState.m_bAltPressed = true;
            bStateChanged = true;
        }
        else if (pHookStruct->vkCode == KEY_SUPER && !keyboardState.m_bSuperPressed)
        {
            keyboardState.m_bSuperPressed = true;
            bStateChanged = true;
        }

        else if (KEY_1 <= pHookStruct->vkCode && pHookStruct->vkCode <= KEY_9 &&
                 keyboardState.m_nNumberPressed == -1)
        {
            keyboardState.m_nNumberPressed = pHookStruct->vkCode - KEY_1 + 1;
            bStateChanged = true;
        }
        else if (KEY_F1 <= pHookStruct->vkCode && pHookStruct->vkCode <= KEY_F9 &&
                 keyboardState.m_nFNumberPressed == -1)
        {
            keyboardState.m_nFNumberPressed = pHookStruct->vkCode - KEY_F1 + 1;
            bStateChanged = true;
        }
        else if (KEY_Q == pHookStruct->vkCode && !keyboardState.m_bQPressed)
        {
            keyboardState.m_bQPressed = true;
            bStateChanged = true;
        }

        // If a valid event, propagate the event to all the running threads.
        if (bStateChanged && keyboardState.m_bAltPressed && keyboardState.m_bSuperPressed)
        {
            if (keyboardState.m_bQPressed)
            {
                // Create the event for toggling.
                const QEvent::Type eType = MAKE_TOGGLE_TYPE();

                // Ship it.
                const std::unordered_set<ClipboardManagerThread*>& lstThreads = g_lstThread;
                for (ClipboardManagerThread* const pThread : lstThreads)
                {
                    QEvent* const pEvent = new QEvent(eType);
                    QCoreApplication::postEvent(pThread, pEvent);
                }
            }
            else if (keyboardState.m_nNumberPressed > 0 || keyboardState.m_nFNumberPressed > 0)
            {
                // Create the event for the slots.
                const quint8 nSlot = keyboardState.m_nNumberPressed > 0
                                         ? keyboardState.m_nNumberPressed
                                         : keyboardState.m_nFNumberPressed;
                const ClipboardManagerThread::ActivationMode eActivationMode =
                    keyboardState.m_nNumberPressed > 0
                        ? ClipboardManagerThread::ACTIVATION_MODE_STORE
                        : ClipboardManagerThread::ACTIVATION_MODE_RETRIEVE;
                const QEvent::Type eType = MAKE_CUSTOM_TYPE(nSlot, eActivationMode);

                // Ship it.
                const std::unordered_set<ClipboardManagerThread*>& lstThreads = g_lstThread;
                for (ClipboardManagerThread* const pThread : lstThreads)
                {
                    QEvent* const pEvent = new QEvent(eType);
                    QCoreApplication::postEvent(pThread, pEvent);
                }
            }

            return true;
        }
    }
    else if (wParam == WM_KEYUP || wParam == WM_SYSKEYUP)
    {
        // Handle the state change of the keys.
        if (pHookStruct->vkCode == KEY_ALT)
        {
            keyboardState.m_bAltPressed = false;
        }
        else if (pHookStruct->vkCode == KEY_SUPER)
        {
            keyboardState.m_bSuperPressed = false;
        }
        else if (KEY_1 <= pHookStruct->vkCode && pHookStruct->vkCode <= KEY_9)
        {
            keyboardState.m_nNumberPressed = -1;
        }
        else if (KEY_F1 <= pHookStruct->vkCode && pHookStruct->vkCode <= KEY_F9)
        {
            keyboardState.m_nFNumberPressed = -1;
        }
        else if (KEY_Q == pHookStruct->vkCode)
        {
            keyboardState.m_bQPressed = false;
        }

        return keyboardState.m_bAltPressed && keyboardState.m_bSuperPressed;
    }

    return false;
}

LRESULT CALLBACK mouseProcedure(int nCode, WPARAM wParam, LPARAM lParam)
{
    if (nCode != HC_ACTION || wParam != WM_MOUSEWHEEL)
    {
        return false;
    }

    // Handle the common part under a lock.
    std::lock_guard lock(g_mutex);
    KeyboardState& keyboardState = g_keyboardState;

    const bool modifiers = keyboardState.m_bSuperPressed && keyboardState.m_bAltPressed;
    if (modifiers)
    {
        const MSLLHOOKSTRUCT* const pMouseInfo = reinterpret_cast<const MSLLHOOKSTRUCT*>(lParam);
        const int64_t nDelta = int16_t(HIWORD(pMouseInfo->mouseData));
        const QEvent::Type eType = MAKE_WHEEL_TYPE(nDelta);

        // Ship it.
        const std::unordered_set<ClipboardManagerThread*>& lstThreads = g_lstThread;
        for (ClipboardManagerThread* const pThread : lstThreads)
        {
            QEvent* const pEvent = new QEvent(eType);
            QCoreApplication::postEvent(pThread, pEvent);
        }
    }

    return false;
}
}  // namespace

class ClipboardManagerThread::PrivateData final
{
public:
    PrivateData() {}
    ~PrivateData() {}

private:
    Q_DISABLE_COPY(PrivateData)
};

ClipboardManagerThread::ClipboardManagerThread() : d(new PrivateData)
{
    moveToThread(this);
    start();
}

ClipboardManagerThread::~ClipboardManagerThread()
{
    // Cancel the loop.
    quit();

    // Wait until finished.
    if (!wait(2000))
    {
        Q_ASSERT_X(0, Q_FUNC_INFO, "Thread shutdown timed out");
        terminate();
    }

    d.reset();
}

bool ClipboardManagerThread::event(QEvent* pEvent)
{
    // Silly check.
    if (!pEvent)
    {
        return false;
    }

    // Only care about user events.
    const QEvent::Type eType = pEvent->type();
    if (IS_TOGGLE_TYPE(eType))
    {
        pEvent->accept();
        emit sig_toggleClipboard();
        return true;
    }
    else if (IS_CUSTOM_TYPE(eType))
    {
        // Extract arguments from user type.
        const ClipboardManagerThread::ActivationMode eActivationMode =
            COMPOUND_MODE_FROM_TYPE(eType);
        const quint8 nSlot = COMPOUND_SLOT_FROM_TYPE(eType);

        // Tell what's just happened (only if really okay).
        if (1 <= nSlot && nSlot <= KEY_COUNT)
        {
            pEvent->accept();
            emit sig_slotActivated(nSlot, eActivationMode);
            return true;
        }
    }
    else if (IS_WHEEL_TYPE(eType))
    {
        if (WHEEL_DIRECTION_FROM_TYPE(eType))
        {
            emit sig_scrollDown();
        }
        else
        {
            emit sig_scrollUp();
        }
    }

    return QThread::event(pEvent);
}

void ClipboardManagerThread::run()
{
    {
        // Handle the common part under a lock.
        std::lock_guard lock(g_mutex);

        // Subscribe the thread for notifications.
        std::unordered_set<ClipboardManagerThread*>& lstThreads = g_lstThread;
        lstThreads.insert(this);

        // If it's the first one, install the hook.
        if (lstThreads.size() == 1)
        {
            KeyboardState& keyboardState = g_keyboardState;
            keyboardState.m_hKeyboardHook =
                SetWindowsHookEx(WH_KEYBOARD_LL, ::keyboardProcedure, GetModuleHandle(nullptr), 0);
            keyboardState.m_hMouseHook =
                SetWindowsHookEx(WH_MOUSE_LL, ::mouseProcedure, GetModuleHandle(nullptr), 0);
        }
    }

    exec();

    {
        // Handle the common part under a lock.
        std::lock_guard lock(g_mutex);

        // Unsubscribe the thread for notifications.
        std::unordered_set<ClipboardManagerThread*>& lstThreads = g_lstThread;
        lstThreads.erase(this);

        // If it's the last one, uninstall the hook.
        if (lstThreads.size())
        {
            KeyboardState& keyboardState = g_keyboardState;
            if (keyboardState.m_hKeyboardHook)
            {
                const bool bUnhooked = UnhookWindowsHookEx(keyboardState.m_hKeyboardHook);
                Q_ASSERT(bUnhooked);
                keyboardState.m_hKeyboardHook = nullptr;
            }
            if (keyboardState.m_hMouseHook)
            {
                const bool bUnhooked = UnhookWindowsHookEx(keyboardState.m_hMouseHook);
                Q_ASSERT(bUnhooked);
                keyboardState.m_hMouseHook = nullptr;
            }
        }
    }
}
