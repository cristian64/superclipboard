#ifndef GLOBALS_H
#define GLOBALS_H

#include <QtCore/QString>

#define APP_NAME QString("SuperClipboard")
#define VERSION QString("2.0")
#define APP_NAME_AND_VERSION QString(APP_NAME + " " + VERSION)

namespace Globals
{
QString getDataDirPath();
}

#endif  // GLOBALS_H
