<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>ClipboardModel</name>
    <message>
        <location filename="../ClipboardModel.cpp" line="189"/>
        <source>Time</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="191"/>
        <source>Tag</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="193"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="195"/>
        <source>Preview</source>
        <translation>Vista previa</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="254"/>
        <source>Invalid column</source>
        <translation>Columna inválida</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="295"/>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="296"/>
        <location filename="../ClipboardModel.cpp" line="488"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="297"/>
        <source>New tag</source>
        <translation>Nueva etiqueta</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="298"/>
        <source>Enter a new tag for the clipboard item.</source>
        <translation>Introduce una nueva etiqueta para el elemento.</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="353"/>
        <source>Retrieve</source>
        <translation>Recuperar</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="356"/>
        <source>Store</source>
        <translation>Almacenar</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="360"/>
        <source>Browse</source>
        <translation>Navegar</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="363"/>
        <source>Edit tag</source>
        <translation>Editar etiqueta</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="366"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="474"/>
        <location filename="../ClipboardModel.cpp" line="841"/>
        <source>Slot %1 retrieved</source>
        <translation>Ranura %1 recuperada</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="483"/>
        <source>Confirm deletion</source>
        <translation>Confirmar borrado</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="484"/>
        <source>Delete selected entries?</source>
        <translation>¿Eliminar las entradas seleccionadas?</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="487"/>
        <source>&amp;Delete</source>
        <translation>&amp;Eliminar</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="564"/>
        <source>Clipboard toggled</source>
        <translation>Portapapeles alternado</translation>
    </message>
    <message>
        <location filename="../ClipboardModel.cpp" line="720"/>
        <source>Slot %1 updated</source>
        <translation>Ranura %1 actualizada</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="41"/>
        <source>Slots</source>
        <translation>Ranuras</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="79"/>
        <source>Slot History</source>
        <translation>Historial de Ranuras</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="117"/>
        <source>History</source>
        <translation>Historial</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="186"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="194"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="200"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="221"/>
        <source>Shortcuts...</source>
        <translation>Accesos directos...</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="230"/>
        <source>About...</source>
        <translation>Acerca de...</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="239"/>
        <source>Restart</source>
        <translation>Reiniciar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="248"/>
        <source>Exit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="257"/>
        <source>Settings</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="272"/>
        <source>Enable</source>
        <translation>Activar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="281"/>
        <source>Show</source>
        <translation>Mostrar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="290"/>
        <source>Hide</source>
        <translation>Ocultar</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="114"/>
        <source>Filter</source>
        <translation>Filtro</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="779"/>
        <source>Confirm exit</source>
        <translation>Confirmar salida</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="780"/>
        <source>Exit application?</source>
        <translation>¿Salir de la aplicación?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="783"/>
        <source>&amp;Exit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="784"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="817"/>
        <source>Built with Qt %1 (running Qt %2).</source>
        <translation>Compilado con Qt %1 (ejecutando Qt %2).</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="819"/>
        <source>%1 is a tool that monitors the system&apos;s clipboard to create clipboard bookmarks and to keep track of the clipboard history.</source>
        <translation>%1 es una herramienta que monitoriza el portapapeles del sistema para almacenar el historial y favoritos.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="824"/>
        <source>About %1</source>
        <translation>Acerca de %1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="90"/>
        <location filename="../MainWindow.cpp" line="825"/>
        <location filename="../MainWindow.cpp" line="883"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="836"/>
        <location filename="../MainWindow.cpp" line="882"/>
        <source>Shortcuts</source>
        <translation>Accesos directos</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="837"/>
        <location filename="../MainWindow.cpp" line="839"/>
        <location filename="../MainWindow.cpp" line="841"/>
        <location filename="../MainWindow.cpp" line="843"/>
        <location filename="../MainWindow.cpp" line="845"/>
        <location filename="../MainWindow.cpp" line="847"/>
        <location filename="../MainWindow.cpp" line="849"/>
        <location filename="../MainWindow.cpp" line="851"/>
        <location filename="../MainWindow.cpp" line="853"/>
        <source>Store into slot %1</source>
        <translation>Almacenar en ranura %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="855"/>
        <location filename="../MainWindow.cpp" line="857"/>
        <location filename="../MainWindow.cpp" line="859"/>
        <location filename="../MainWindow.cpp" line="861"/>
        <location filename="../MainWindow.cpp" line="863"/>
        <location filename="../MainWindow.cpp" line="865"/>
        <location filename="../MainWindow.cpp" line="867"/>
        <location filename="../MainWindow.cpp" line="869"/>
        <location filename="../MainWindow.cpp" line="871"/>
        <source>Retrieve from slot %1</source>
        <translation>Recuperar la ranura %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="873"/>
        <source>Navigate history</source>
        <translation>Navegar historial</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="878"/>
        <source>Toggle clipboard</source>
        <translation>Alternar portapapeles</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../main.cpp" line="83"/>
        <source>Another instance is already running. If you think this isn&apos;t the case, check the lock file&apos;s permissions: &quot;%1&quot;</source>
        <translation>Otra instancia de la aplicación ya está abierta. Si crees que se trata de un error, confirma los permisos del fichero: &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../SettingsDialog.ui" line="14"/>
        <source>Settings</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="23"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="29"/>
        <source>Settings location:</source>
        <translation>Localización de datos</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="46"/>
        <source>Data location*:</source>
        <translation>Directorio de datos*:</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="58"/>
        <source>&amp;Browse</source>
        <translation>&amp;Explorar</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="70"/>
        <source>Max. history count:</source>
        <translation>Máximo número de entradas en el historial:</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="102"/>
        <source>Pattern Substitutions</source>
        <translation>Sustitución de Patrones</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="108"/>
        <source>A ECMAScript pattern per line.</source>
        <translation>Un patrón en formato ECMAScript en cada línea</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="111"/>
        <source>Enter a ECMAScript pattern per line</source>
        <translation>Introduce un patrón en formato ECMAScript en cada línea</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="118"/>
        <source>The corresponding substitution pattern for each line.</source>
        <translation>El patrón de sustitución correspondiente con cada línea.</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="121"/>
        <source>Enter the corresponding substitution pattern for each line</source>
        <translation>Introduce el patrón de sustitución correspondiente con cada línea</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="131"/>
        <source>Behaviour</source>
        <translation>Comportamiento</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="137"/>
        <source>Show popups from system tray icon</source>
        <translation>Mostrar popups desde el icono de la bandeja del sistema</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="144"/>
        <source>Show notifications</source>
        <translation>Mostrar notificaciones</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="151"/>
        <source>Show system tray icon</source>
        <translation>Mostrar icono de la bandeja del sistema</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="158"/>
        <source>Ask for confirmation when deleting items</source>
        <translation>Pedir confirmación al eliminar entradas</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="175"/>
        <source>* Some settings will be applied only after restart</source>
        <translation>* Algunas opciones se aplicarán después de reiniciar</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="195"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
</context>
</TS>
