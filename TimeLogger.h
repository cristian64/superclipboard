#ifndef TIMELOGGER_H
#define TIMELOGGER_H

#include <QtCore/QDateTime>
#include <QtCore/QDebug>
#include <QtCore/QString>

class TimeLogger final
{
public:
    TimeLogger(const QString& strMessage)
        : m_strMessage(strMessage), m_nMsecElapsedSinceEpoch(QDateTime::currentMSecsSinceEpoch())
    {
    }

    ~TimeLogger()
    {
        const qint64 nMsecElapsed = QDateTime::currentMSecsSinceEpoch() - m_nMsecElapsedSinceEpoch;
        qWarning() << m_strMessage.arg(nMsecElapsed);
    }

private:
    const QString m_strMessage;
    const qint64 m_nMsecElapsedSinceEpoch;

    Q_DISABLE_COPY(TimeLogger)
};

#endif  // TIMELOGGER_H
