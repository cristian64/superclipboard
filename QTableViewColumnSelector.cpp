#include "QTableViewColumnSelector.h"

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
#include <QtGui/QAction>
#else
#include <QtWidgets/QAction>
#endif
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMenu>

#include "Utils.h"

QTableViewColumnSelector::QTableViewColumnSelector() : m_pTableView(nullptr) {}

QTableViewColumnSelector::~QTableViewColumnSelector()
{
    m_pTableView = nullptr;
}

void QTableViewColumnSelector::setTableView(QTableView* pTableView)
{
    // Disconnect previous table view.
    {
        QHeaderView* const pHeaderView = m_pTableView ? m_pTableView->horizontalHeader() : nullptr;
        if (pHeaderView)
        {
            pHeaderView->setContextMenuPolicy(Qt::DefaultContextMenu);
            Q_DISCONNECT(pHeaderView,
                         SIGNAL(customContextMenuRequested(const QPoint&)),
                         this,
                         SLOT(slt_customContextMenuRequested(const QPoint&)));
            m_pTableView = nullptr;
        }
    }

    // Connect new table view.
    {
        QHeaderView* const pHeaderView = pTableView ? pTableView->horizontalHeader() : nullptr;
        if (pHeaderView)
        {
            pHeaderView->setContextMenuPolicy(Qt::CustomContextMenu);
            Q_CONNECT(pHeaderView,
                      SIGNAL(customContextMenuRequested(const QPoint&)),
                      this,
                      SLOT(slt_customContextMenuRequested(const QPoint&)));
            m_pTableView = pTableView;
        }
    }
}

void QTableViewColumnSelector::slt_customContextMenuRequested(const QPoint& ptPos)
{
    Q_UNUSED(ptPos);

    const QAbstractItemModel* const pModel = m_pTableView ? m_pTableView->model() : nullptr;
    if (!pModel)
    {
        return;
    }

    QMenu menu;

    const int nColumnCount = pModel->columnCount();
    for (int i = 0; i < nColumnCount; ++i)
    {
        const QString strColumnName = pModel->headerData(i, Qt::Horizontal).toString();
        const bool bColumnVisible = !m_pTableView->isColumnHidden(i);

        QAction* const pAction = menu.addAction(strColumnName);
        pAction->setCheckable(true);
        pAction->setChecked(bColumnVisible);
        pAction->setProperty("column", i);
    }

    menu.show();
    const QAction* const pAction = menu.exec(QCursor::pos());
    if (!pAction)
    {
        return;
    }

    const QVariant varColumn = pAction->property("column");
    if (varColumn.isNull())
    {
        return;
    }

    m_pTableView->setColumnHidden(varColumn.toInt(), !pAction->isChecked());
}
