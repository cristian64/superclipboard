#include "MainWindow.h"

#include <QtCore/QDebug>
#include <QtCore/QTimer>
#include <QtGui/QCloseEvent>
#include <QtGui/QPixmap>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSystemTrayIcon>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>

#include "ClipboardCache.h"
#include "ClipboardItem.h"
#include "ClipboardManager.h"
#include "ClipboardModel.h"
#include "Globals.h"
#include "Notification.h"
#include "QMimeDataEx.h"
#include "QTableViewColumnSelector.h"
#include "Settings.h"
#include "SettingsDialog.h"
#include "Utils.h"
#include "ui_MainWindow.h"

#ifdef WIN32
#include <windows.h>
#endif

#define DEFAULT_FRAME_STYLE "background: #AAA"

namespace
{
void setSystemTrayIcon(QSystemTrayIcon& systemTrayIcon, const QIcon& icon)
{
#ifdef WIN32
    // Since Qt is messing up the icon size on Windows,
    // we want to calculate it ourselves.
    const auto nWidth = GetSystemMetrics(SM_CXSMICON);
    const auto nHeight = GetSystemMetrics(SM_CYSMICON);
    const QPixmap pixmap = icon.pixmap(icon.actualSize(QSize(nWidth, nHeight)));
    systemTrayIcon.setIcon(pixmap);
#else
    systemTrayIcon.setIcon(icon);
#endif
}
}  // namespace

class MainWindow::PrivateData final
{
public:
    PrivateData()
        : m_pLineEdit(nullptr),
          m_clipboardModelHistory(ClipboardModel::CLIPBOARD_HISTORY),
          m_clipboardModelSlotHistory(ClipboardModel::CLIPBOARD_SLOT_HISTORY),
          m_clipboardModelSlots(ClipboardModel::CLIPBOARD_SLOTS),
          m_nApplicationState(Qt::ApplicationState::ApplicationSuspended),
          m_nApplicationStateTime(0)
    {
    }

    ~PrivateData() {}

    Ui::MainWindow ui;
    QLineEdit* m_pLineEdit;

    QSystemTrayIcon m_systemTrayIcon;

    ClipboardCache m_clipboardCache;

    ClipboardModel m_clipboardModelHistory;
    ClipboardModel m_clipboardModelSlotHistory;
    ClipboardModel m_clipboardModelSlots;

    QTableViewColumnSelector m_columnSelectorSlots;
    QTableViewColumnSelector m_columnSelectorHistory;
    QTableViewColumnSelector m_columnSelectorSlotHistory;

    Qt::ApplicationState m_nApplicationState;
    qint64 m_nApplicationStateTime;

    QPair<qint64, QPair<const ClipboardModel*, quint32>> m_pairItemLastPreviewed;
    QByteArray m_baLastValidGeometry;

private:
    Q_DISABLE_COPY(PrivateData)
};

static MainWindow* g_pInstance = nullptr;

MainWindow& MainWindow::instance()
{
    Q_ASSERT(g_pInstance);
    return *g_pInstance;
}

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), d(new PrivateData)
{
    Q_ASSERT(!g_pInstance);
    g_pInstance = this;

    // Set up main window.
    d->ui.setupUi(this);
    setWindowIcon(QIcon(":/resources/clipboard.svg"));
    setWindowTitle(APP_NAME);
    d->ui.frame->setStyleSheet(DEFAULT_FRAME_STYLE);

    // Add a text field for the filter. It was a bit tricky
    // for the .ui file.
    d->m_pLineEdit = new QLineEdit(d->ui.tabWidget);
    d->m_pLineEdit->setPlaceholderText(tr("Filter"));
    d->ui.tabWidget->setCornerWidget(d->m_pLineEdit);
    Q_CONNECT(d->m_pLineEdit,
              SIGNAL(textChanged(const QString&)),
              &d->m_clipboardModelSlots,
              SLOT(slt_filterChanged(const QString&)));
    Q_CONNECT(d->m_pLineEdit,
              SIGNAL(textChanged(const QString&)),
              &d->m_clipboardModelSlotHistory,
              SLOT(slt_filterChanged(const QString&)));
    Q_CONNECT(d->m_pLineEdit,
              SIGNAL(textChanged(const QString&)),
              &d->m_clipboardModelHistory,
              SLOT(slt_filterChanged(const QString&)));
    QAction* pClearFilterAction =
        d->m_pLineEdit->addAction(QIcon(":/resources/clear.svg"), QLineEdit::TrailingPosition);
    Q_CONNECT(pClearFilterAction, &QAction::triggered, this, &MainWindow::slt_clearFilter);

    // Set up system tray icon.
    ::setSystemTrayIcon(d->m_systemTrayIcon, windowIcon());
    if (SETTING("Layout/system_tray_icon_visible", true).toBool())
    {
        d->m_systemTrayIcon.show();
    }
    d->m_systemTrayIcon.setContextMenu(d->ui.menuSystemTray);
    d->m_systemTrayIcon.setToolTip(APP_NAME);
    Q_CONNECT(&d->m_systemTrayIcon,
              SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
              this,
              SLOT(slt_systemTrayIconActivated(QSystemTrayIcon::ActivationReason)));
    Q_CONNECT(
        &d->m_clipboardModelHistory,
        SIGNAL(sig_showMessage(const QString&, const QString&, QSystemTrayIcon::MessageIcon, int)),
        this,
        SLOT(slt_showMessage(const QString&, const QString&, QSystemTrayIcon::MessageIcon, int)));
    Q_CONNECT(
        &d->m_clipboardModelSlotHistory,
        SIGNAL(sig_showMessage(const QString&, const QString&, QSystemTrayIcon::MessageIcon, int)),
        this,
        SLOT(slt_showMessage(const QString&, const QString&, QSystemTrayIcon::MessageIcon, int)));
    Q_CONNECT(
        &d->m_clipboardModelSlots,
        SIGNAL(sig_showMessage(const QString&, const QString&, QSystemTrayIcon::MessageIcon, int)),
        this,
        SLOT(slt_showMessage(const QString&, const QString&, QSystemTrayIcon::MessageIcon, int)));

    // For the History model, we want to hide non-highlighted rows.
    ClipboardSortFilterProxyModel* const pClipboardProxyModelHistory =
        new ClipboardSortFilterProxyModel(this);
    pClipboardProxyModelHistory->setSourceModel(&d->m_clipboardModelHistory);

    // Set the model for all 3 views.
    d->ui.tableViewSlots->setModel(&d->m_clipboardModelSlots);
    d->ui.tableViewSlotHistory->setModel(&d->m_clipboardModelSlotHistory);
    d->ui.tableViewHistory->setModel(pClipboardProxyModelHistory);

    // Set the column selectors.
    d->m_columnSelectorSlots.setTableView(d->ui.tableViewSlots);
    d->m_columnSelectorSlotHistory.setTableView(d->ui.tableViewSlotHistory);
    d->m_columnSelectorHistory.setTableView(d->ui.tableViewHistory);

    // Handle the context menu.
    d->ui.tableViewSlots->setContextMenuPolicy(Qt::CustomContextMenu);
    d->ui.tableViewSlotHistory->setContextMenuPolicy(Qt::CustomContextMenu);
    d->ui.tableViewHistory->setContextMenuPolicy(Qt::CustomContextMenu);
    Q_CONNECT(d->ui.tableViewSlots,
              SIGNAL(customContextMenuRequested(const QPoint&)),
              this,
              SLOT(slt_customContextMenuRequested(const QPoint&)));
    Q_CONNECT(d->ui.tableViewSlotHistory,
              SIGNAL(customContextMenuRequested(const QPoint&)),
              this,
              SLOT(slt_customContextMenuRequested(const QPoint&)));
    Q_CONNECT(d->ui.tableViewHistory,
              SIGNAL(customContextMenuRequested(const QPoint&)),
              this,
              SLOT(slt_customContextMenuRequested(const QPoint&)));

    // Handle the events.
    d->ui.tableViewSlots->installEventFilter(this);
    d->ui.tableViewSlotHistory->installEventFilter(this);
    d->ui.tableViewHistory->installEventFilter(this);

    // Handle the rest of the signals.
    Q_CONNECT(
        d->ui.tableViewSlots, SIGNAL(clicked(QModelIndex)), this, SLOT(slt_clicked(QModelIndex)));
    Q_CONNECT(d->ui.tableViewSlotHistory,
              SIGNAL(clicked(QModelIndex)),
              this,
              SLOT(slt_clicked(QModelIndex)));
    Q_CONNECT(
        d->ui.tableViewHistory, SIGNAL(clicked(QModelIndex)), this, SLOT(slt_clicked(QModelIndex)));
    Q_CONNECT(d->ui.tableViewSlots,
              SIGNAL(doubleClicked(QModelIndex)),
              this,
              SLOT(slt_doubleClicked(QModelIndex)));
    Q_CONNECT(d->ui.tableViewSlotHistory,
              SIGNAL(doubleClicked(QModelIndex)),
              this,
              SLOT(slt_doubleClicked(QModelIndex)));
    Q_CONNECT(d->ui.tableViewHistory,
              SIGNAL(doubleClicked(QModelIndex)),
              this,
              SLOT(slt_doubleClicked(QModelIndex)));
    Q_CONNECT(d->ui.tableViewSlots->selectionModel(),
              SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
              this,
              SLOT(slt_currentRowChanged(QModelIndex, QModelIndex)));
    Q_CONNECT(d->ui.tableViewSlotHistory->selectionModel(),
              SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
              this,
              SLOT(slt_currentRowChanged(QModelIndex, QModelIndex)));
    Q_CONNECT(d->ui.tableViewHistory->selectionModel(),
              SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
              this,
              SLOT(slt_currentRowChanged(QModelIndex, QModelIndex)));
    Q_CONNECT(d->ui.tableViewSlots->selectionModel(),
              SIGNAL(currentColumnChanged(QModelIndex, QModelIndex)),
              this,
              SLOT(slt_currentColumnChanged(QModelIndex, QModelIndex)));
    Q_CONNECT(d->ui.tableViewSlotHistory->selectionModel(),
              SIGNAL(currentColumnChanged(QModelIndex, QModelIndex)),
              this,
              SLOT(slt_currentColumnChanged(QModelIndex, QModelIndex)));
    Q_CONNECT(d->ui.tableViewHistory->selectionModel(),
              SIGNAL(currentColumnChanged(QModelIndex, QModelIndex)),
              this,
              SLOT(slt_currentColumnChanged(QModelIndex, QModelIndex)));

    // Set common properties for all 3 views.
    for (int i = ClipboardModel::COLUMN_FIRST; i < ClipboardModel::COLUMN_COUNT; ++i)
    {
        const QHeaderView::ResizeMode eResizeMode =
            d->m_clipboardModelSlots.getResizeMode(i, Qt::Horizontal);
        d->ui.tableViewSlots->horizontalHeader()->setSectionResizeMode((ClipboardModel::Columns)i,
                                                                       eResizeMode);
        d->ui.tableViewSlotHistory->horizontalHeader()->setSectionResizeMode(
            (ClipboardModel::Columns)i, eResizeMode);
        d->ui.tableViewHistory->horizontalHeader()->setSectionResizeMode((ClipboardModel::Columns)i,
                                                                         eResizeMode);
    }

    // For the slots view, where the number of rows is fixed to a small number,
    // make the rows fill the entire view.
    d->ui.tableViewSlots->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    d->ui.tableViewSlotHistory->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    d->ui.tableViewHistory->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    d->ui.tableViewSlotHistory->verticalHeader()->setDefaultSectionSize(
        d->ui.tableViewSlotHistory->verticalHeader()->minimumSectionSize() + 5);
    d->ui.tableViewHistory->verticalHeader()->setDefaultSectionSize(
        d->ui.tableViewHistory->verticalHeader()->minimumSectionSize() + 5);

    // Connect other signals.
    Q_CONNECT(d->ui.actionRestart, SIGNAL(triggered(bool)), this, SLOT(slt_restartTriggered(bool)));
    Q_CONNECT(d->ui.actionExit, SIGNAL(triggered(bool)), this, SLOT(slt_exitTriggered(bool)));
    Q_CONNECT(d->ui.actionEnable, SIGNAL(toggled(bool)), this, SLOT(slt_enableToggled(bool)));
    Q_CONNECT(d->ui.actionAbout, SIGNAL(triggered(bool)), this, SLOT(slt_aboutTriggered(bool)));
    Q_CONNECT(
        d->ui.actionShortcuts, SIGNAL(triggered(bool)), this, SLOT(slt_shortcutsTriggered(bool)));
    Q_CONNECT(
        d->ui.actionSettings, SIGNAL(triggered(bool)), this, SLOT(slt_settingsTriggered(bool)));
    Q_CONNECT(d->ui.actionShow, SIGNAL(triggered(bool)), this, SLOT(slt_showTriggered(bool)));
    Q_CONNECT(d->ui.actionHide, SIGNAL(triggered(bool)), this, SLOT(slt_hideTriggered(bool)));

    Q_CONNECT(qApp,
              SIGNAL(applicationStateChanged(Qt::ApplicationState)),
              this,
              SLOT(slt_applicationStateChanged(Qt::ApplicationState)));

    restoreLayout();
}

MainWindow::~MainWindow()
{
    Q_ASSERT(g_pInstance == this);
    g_pInstance = nullptr;

    d.reset();
}

QSystemTrayIcon& MainWindow::getSystemTrayIconRef()
{
    return d->m_systemTrayIcon;
}

void MainWindow::blinkDisabledSystemTrayIcon()
{
    slt_systemTrayIconDisabledExcited(true);
}

void MainWindow::saveLayout() const
{
    SETTINGS_GROUP("Layout");

    SET_SETTING("geometry", isVisible() ? saveGeometry() : d->m_baLastValidGeometry);
    SET_SETTING("state", saveState());
    SET_SETTING("visible", isVisible());
    SET_SETTING("enabled", ClipboardManager::instance().isEnabled());
    SET_SETTING("filter", d->m_pLineEdit->text());
    SET_SETTING("system_tray_icon_visible", d->m_systemTrayIcon.isVisible());
    SET_SETTING("current_tab_index", d->ui.tabWidget->currentIndex());
    SET_SETTING("splitter_state", d->ui.splitter->saveState());
    SET_SETTING("history_state", d->ui.tableViewHistory->horizontalHeader()->saveState());
    SET_SETTING("slots_state", d->ui.tableViewSlots->horizontalHeader()->saveState());
    SET_SETTING("slot_history_state", d->ui.tableViewSlotHistory->horizontalHeader()->saveState());
}

void MainWindow::restoreLayout()
{
    SETTINGS_GROUP("Layout");

    d->m_baLastValidGeometry = SETTING("geometry").toByteArray();
    if (d->m_baLastValidGeometry.size())
    {
        restoreGeometry(d->m_baLastValidGeometry);
    }
    restoreState(SETTING("state").toByteArray());
    setVisible(SETTING("visible", true).toBool());
    d->m_pLineEdit->setText(SETTING("filter").toString());
    d->ui.actionEnable->setChecked(SETTING("enabled", true).toBool());
    d->m_systemTrayIcon.setVisible(SETTING("system_tray_icon_visible", true).toBool());
    d->ui.tabWidget->setCurrentIndex(SETTING("current_tab_index").toInt());
    const QByteArray baSplitterState = SETTING("splitter_state").toByteArray();
    if (baSplitterState.size())
    {
        d->ui.splitter->restoreState(baSplitterState);
    }
    else if (d->ui.splitter->sizes().size() == 2)
    {
        d->ui.splitter->setSizes(QList<int>() << 100 << 100);
    }
    Q_ASSERT(d->ui.splitter->sizes().size() == 2);
    d->ui.tableViewHistory->horizontalHeader()->restoreState(
        SETTING("history_state").toByteArray());
    d->ui.tableViewSlots->horizontalHeader()->restoreState(SETTING("slots_state").toByteArray());
    d->ui.tableViewSlotHistory->horizontalHeader()->restoreState(
        SETTING("slot_history_state").toByteArray());
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    event->ignore();

    // If it isn't available, treat it as exit.
    if (!d->m_systemTrayIcon.isSystemTrayAvailable() || !d->m_systemTrayIcon.isVisible())
    {
        slt_exitTriggered(false);
        return;
    }

    // Do not really exit but minimize.
    if (isVisible())
    {
        // Keep a copy of the geometry now that we know it's valid.
        d->m_baLastValidGeometry = saveGeometry();
        hide();
    }
}

void MainWindow::slt_systemTrayIconActivated(QSystemTrayIcon::ActivationReason eActivationReason)
{
    // Keep a copy of the geometry now, when it is reliable.
    if (isVisible())
    {
        d->m_baLastValidGeometry = saveGeometry();
    }

    // Before displaying the system tray menu, update the visibility of the hide
    // action.
    if (eActivationReason == QSystemTrayIcon::Context)
    {
        d->ui.actionHide->setEnabled(isVisible());
    }
    else
    {
        // And, for the left mouse click, toggle visibility of the window.
        const qint64 nNow = QDateTime::currentMSecsSinceEpoch();
        const qint64 nElapsedSinceAppStateChange = nNow - d->m_nApplicationStateTime;
        const bool bApplicationStateReliable = nElapsedSinceAppStateChange > 300;
        if (!isVisible() || isHidden() || isMinimized() ||
            (bApplicationStateReliable && d->m_nApplicationState != Qt::ApplicationActive))
        {
            slt_raiseWindow();
        }
        else
        {
            slt_hideWindow();
        }
    }
}

void MainWindow::slt_customContextMenuRequested(const QPoint& ptPos)
{
    Q_UNUSED(ptPos);

    // Is it a expected sender?
    const QObject* const pSender = sender();
    if (pSender != d->ui.tableViewSlots && pSender != d->ui.tableViewSlotHistory &&
        pSender != d->ui.tableViewHistory)
    {
        return;
    }

    // Just double check.
    const QTableView* const pTableView = dynamic_cast<const QTableView* const>(pSender);
    if (!pTableView)
    {
        return;
    }
    QSortFilterProxyModel* const pFilterProxyModel =
        dynamic_cast<QSortFilterProxyModel* const>(pTableView->model());
    ClipboardModel* const pModel =
        dynamic_cast<ClipboardModel* const>(pTableView->model())
            ? dynamic_cast<ClipboardModel* const>(pTableView->model())
            : dynamic_cast<ClipboardModel* const>(pFilterProxyModel->sourceModel());
    if (!pModel)
    {
        return;
    }

    // Show the context menu.
    const QModelIndexList indexes = pTableView->selectionModel()->selection().indexes();
    QSet<quint32> nlstIndexes;
    for (const QModelIndex& index : indexes)
    {
        nlstIndexes << static_cast<quint32>(
            pFilterProxyModel ? pFilterProxyModel->mapToSource(index).row() : index.row());
    }
    pModel->showCustomContextMenu(nlstIndexes.values());
}

void MainWindow::slt_currentRowChanged(const QModelIndex& modelIndex,
                                       const QModelIndex& previousIndex)
{
    Q_UNUSED(previousIndex);

    // Is it a expected sender?
    const QObject* const pSender = sender();
    if (pSender != d->ui.tableViewSlots->selectionModel() &&
        pSender != d->ui.tableViewSlotHistory->selectionModel() &&
        pSender != d->ui.tableViewHistory->selectionModel())
    {
        return;
    }

    // Just double check.
    const QItemSelectionModel* const pItemSelectionModel =
        dynamic_cast<const QItemSelectionModel* const>(pSender);
    if (!pItemSelectionModel)
    {
        return;
    }

    const QSortFilterProxyModel* const pFilterProxyModel =
        dynamic_cast<const QSortFilterProxyModel* const>(pItemSelectionModel->model());
    const ClipboardModel* const pModel =
        dynamic_cast<const ClipboardModel* const>(pItemSelectionModel->model())
            ? dynamic_cast<const ClipboardModel* const>(pItemSelectionModel->model())
            : dynamic_cast<const ClipboardModel* const>(pFilterProxyModel->sourceModel());

    showPreview(
        pModel,
        static_cast<quint32>(pFilterProxyModel ? pFilterProxyModel->mapToSource(modelIndex).row()
                                               : modelIndex.row()));
}

void MainWindow::slt_currentColumnChanged(const QModelIndex& modelIndex,
                                          const QModelIndex& previousIndex)
{
    Q_UNUSED(previousIndex);

    // Is it a expected sender?
    QObject* const pSender = sender();
    if (pSender != d->ui.tableViewSlots->selectionModel() &&
        pSender != d->ui.tableViewSlotHistory->selectionModel() &&
        pSender != d->ui.tableViewHistory->selectionModel())
    {
        return;
    }

    // Just double check.
    QItemSelectionModel* const pItemSelectionModel = dynamic_cast<QItemSelectionModel*>(pSender);
    if (!pItemSelectionModel)
    {
        return;
    }

    // TODO(CA): In Qt 5.9.9, Qt 5.12.11, Qt 5.15.2, Qt annoyingly scrolls to
    // the current cell. This is really undesired when the user simply wants to
    // preview an item by clicking on the **Preview** column.
    // This is new behavior in Qt, it has not been always like that.
    // Interestingly, Qt 6 (6.0.4, 6.1.3, 6.2.0 Beta3) is not yet affected. The
    // issue might have been fixed there, or might have not been cherry-picked
    // yet. For now, we are going to connect to `currentColumnChanged()` and
    // force the current item to be the first column.
    // This prevents sideways navigation with keyboard arrows, but it's not
    // really relevant in this application, as our selection behavior is
    // configured as "rows" (see `QAbstractItemView::SelectRows` in UI file).
    // In time, once we know if Qt 6 is going to adopt this behavior or not, we
    // should remove this hack, or make it official.
    if (modelIndex.column() != ClipboardModel::COLUMN_FIRST)
    {
        pItemSelectionModel->setCurrentIndex(
            modelIndex.sibling(modelIndex.row(), ClipboardModel::COLUMN_FIRST),
            QItemSelectionModel::Current);
    }
}

void MainWindow::slt_doubleClicked(const QModelIndex& modelIndex)
{
    // Is it a expected sender?
    const QObject* const pSender = sender();
    if (pSender != d->ui.tableViewSlots && pSender != d->ui.tableViewSlotHistory &&
        pSender != d->ui.tableViewHistory)
    {
        return;
    }

    // Just double check.
    const QTableView* const pTableView = dynamic_cast<const QTableView* const>(pSender);
    if (!pTableView)
    {
        return;
    }
    const QSortFilterProxyModel* const pFilterProxyModel =
        dynamic_cast<const QSortFilterProxyModel* const>(pTableView->model());
    ClipboardModel* const pModel =
        dynamic_cast<ClipboardModel* const>(pTableView->model())
            ? dynamic_cast<ClipboardModel* const>(pTableView->model())
            : dynamic_cast<ClipboardModel* const>(pFilterProxyModel->sourceModel());

    const quint32 row = static_cast<quint32>(
        pFilterProxyModel ? pFilterProxyModel->mapToSource(modelIndex).row() : modelIndex.row());
    if (modelIndex.column() == ClipboardModel::COLUMN_TAG)
    {
        pModel->showEditTagDialog(row);
    }
    else if (modelIndex.column() == ClipboardModel::COLUMN_PREVIEW)
    {
        pModel->slt_clipboardRequested(row);
    }
}

void MainWindow::slt_clicked(const QModelIndex& modelIndex)
{
    // Is it a expected sender?
    const QObject* const pSender = sender();
    if (pSender != d->ui.tableViewSlots && pSender != d->ui.tableViewSlotHistory &&
        pSender != d->ui.tableViewHistory)
    {
        return;
    }

    // Just double check.
    const QTableView* const pTableView = dynamic_cast<const QTableView* const>(pSender);
    if (!pTableView)
    {
        return;
    }
    const QSortFilterProxyModel* const pFilterProxyModel =
        dynamic_cast<const QSortFilterProxyModel* const>(pTableView->model());
    const ClipboardModel* const pModel =
        dynamic_cast<ClipboardModel* const>(pTableView->model())
            ? dynamic_cast<ClipboardModel* const>(pTableView->model())
            : dynamic_cast<ClipboardModel* const>(pFilterProxyModel->sourceModel());

    showPreview(
        pModel,
        static_cast<quint32>(pFilterProxyModel ? pFilterProxyModel->mapToSource(modelIndex).row()
                                               : modelIndex.row()));
}

void MainWindow::showPreview(const ClipboardModel* const pModel, const quint32 nIndex)
{
    // Prevent from several events in the same tick (row changed? mouse
    // clicked?).
    const qint64 nLastPreviewed = QDateTime::currentMSecsSinceEpoch();
    const QPair<const ClipboardModel*, quint32> pairItemToPreview(pModel, nIndex);
    const bool bEnoughTimeHasElapsed = nLastPreviewed - 1000 > d->m_pairItemLastPreviewed.first ||
                                       nLastPreviewed < d->m_pairItemLastPreviewed.first;
    if (pairItemToPreview == d->m_pairItemLastPreviewed.second && !bEnoughTimeHasElapsed)
    {
        return;
    }
    d->m_pairItemLastPreviewed =
        QPair<qint64, QPair<const ClipboardModel*, quint32>>(nLastPreviewed, pairItemToPreview);

    // Remove any possible widget from the frame.
    QLayoutItem* pLayoutItem;
    while ((pLayoutItem = d->ui.frame->layout()->takeAt(0)))
    {
        delete pLayoutItem->widget();
        delete pLayoutItem;
    }

    do
    {
        // Bail if not even visible.
        Q_ASSERT_X(
            d->ui.splitter->sizes().size() == 2, Q_FUNC_INFO, "There should be two sides only");
        if (d->ui.splitter->sizes().size() != 2)
        {
            break;
        }
        if (d->ui.splitter->sizes()[1] == 0)
        {
            break;
        }
        if (!d->ui.frame->isVisible())
        {
            break;
        }

        // Just double check.
        if (!pModel)
        {
            break;
        }

        // Now we have an item that should be fully loaded.
        const ClipboardItem* const pItem = pModel->getClipboardItem(nIndex);
        if (!pItem)
        {
            break;
        }

        // Show in the preview frame.
        const QMimeData& mimeData = pItem->m_mimeData;
        if (mimeData.hasHtml())
        {
            QTextBrowser* const pTextBrowser = new QTextBrowser();
            pTextBrowser->setHtml(mimeData.html());
            d->ui.frame->layout()->addWidget(pTextBrowser);
        }
        else if (mimeData.hasText())
        {
            QTextEdit* const pTextEdit = new QTextEdit();
            pTextEdit->setText(mimeData.text());
            d->ui.frame->layout()->addWidget(pTextEdit);
        }
        else if (mimeData.hasUrls())
        {
            const QList<QUrl> lstUrls = mimeData.urls();
            QStringList strlstUrls;
            for (const QUrl& url : lstUrls)
            {
                strlstUrls << url.toString();
            }

            QTextEdit* const pTextEdit = new QTextEdit();
            pTextEdit->setText(strlstUrls.join("\n"));
            d->ui.frame->layout()->addWidget(pTextEdit);
        }
        else if (mimeData.hasImage())
        {
            QImage image;
            QMimeDataEx::getImage(mimeData, image);
            if (!image.width() || !image.height())
            {
                break;
            }

            QScrollArea* const pScrollArea = new QScrollArea();
            pScrollArea->setAlignment(Qt::AlignCenter | Qt::AlignHCenter);
            QLabel* const pLabel = new QLabel(pScrollArea);
            pLabel->setPixmap(QPixmap::fromImage(image));
            pLabel->setAlignment(Qt::AlignCenter | Qt::AlignHCenter);
            pScrollArea->setWidget(pLabel);
            d->ui.frame->layout()->addWidget(pScrollArea);
        }
        else
        {
            break;
        }

        // If we got something added, clear the background darkness.
        d->ui.frame->setStyleSheet("");

        return;
    } while (false);

    // We got nothing? Put something dark.
    d->ui.frame->setStyleSheet(DEFAULT_FRAME_STYLE);
}

bool MainWindow::eventFilter(QObject* pWatched, QEvent* pEvent)
{
    if (!pEvent)
    {
        return false;
    }

    // Is it a expected sender?
    if (pWatched != d->ui.tableViewSlots && pWatched != d->ui.tableViewSlotHistory &&
        pWatched != d->ui.tableViewHistory)
    {
        return false;
    }

    // Just double check.
    const QTableView* const pTableView = dynamic_cast<const QTableView* const>(pWatched);
    if (!pTableView)
    {
        return false;
    }
    QSortFilterProxyModel* const pFilterProxyModel =
        dynamic_cast<QSortFilterProxyModel* const>(pTableView->model());
    ClipboardModel* const pModel =
        dynamic_cast<ClipboardModel* const>(pTableView->model())
            ? dynamic_cast<ClipboardModel* const>(pTableView->model())
            : dynamic_cast<ClipboardModel* const>(pFilterProxyModel->sourceModel());
    if (!pModel)
    {
        return false;
    }

    if (pEvent->type() == QEvent::KeyPress)
    {
        switch (static_cast<QKeyEvent*>(pEvent)->key())
        {
        case Qt::Key_Enter:
        case Qt::Key_Return:
        {
            const QModelIndex currentIndex = pTableView->selectionModel()->currentIndex();
            pModel->slt_clipboardRequested(static_cast<quint32>(
                pFilterProxyModel ? pFilterProxyModel->mapToSource(currentIndex).row()
                                  : currentIndex.row()));
            return true;
        }
        case Qt::Key_Delete:
        case Qt::Key_Backspace:
        {
            const QList<QModelIndex> lstCurrentIndexes =
                pTableView->selectionModel()->selectedRows();
            QList<quint32> lstIndexes;
            for (const QModelIndex& modelIndex : lstCurrentIndexes)
            {
                lstIndexes << static_cast<quint32>(
                    pFilterProxyModel ? pFilterProxyModel->mapToSource(modelIndex).row()
                                      : modelIndex.row());
            }
            pModel->slt_clipboardDelete(lstIndexes);
            return true;
        }
        }
    }

    return false;
}

void MainWindow::slt_restartTriggered(const bool bChecked)
{
    Q_UNUSED(bChecked);

    d->m_clipboardCache.purgeCache();

    saveLayout();
    qApp->exit(c_nRestartExitCode);
}

void MainWindow::slt_exitTriggered(bool bChecked /* = false*/)
{
    Q_UNUSED(bChecked);

    QMessageBox messageBox(QMessageBox::Icon::Question,
                           tr("Confirm exit"),
                           tr("Exit application?"),
                           QMessageBox::NoButton,
                           &MainWindow::instance());
    QPushButton exitButton(tr("&Exit"), &messageBox);
    QPushButton cancelButton(tr("&Cancel"), &messageBox);
    messageBox.addButton(&exitButton, QMessageBox::AcceptRole);
    messageBox.addButton(&cancelButton, QMessageBox::RejectRole);
    messageBox.setDefaultButton(&exitButton);
    messageBox.setEscapeButton(&cancelButton);
    messageBox.exec();
    if (messageBox.clickedButton() != &exitButton)
    {
        return;
    }

    saveLayout();

    QApplication::exit();
}

void MainWindow::slt_enableToggled(bool bChecked /* = false*/)
{
    ClipboardManager::instance().setEnabled(bChecked);

    static const QIcon icDisabled(":/resources/clipboard_disabled.svg");
    ::setSystemTrayIcon(d->m_systemTrayIcon, bChecked ? windowIcon() : icDisabled);
}

void MainWindow::slt_aboutTriggered(bool bChecked /* = false*/)
{
    Q_UNUSED(bChecked);

    QString strMessage;
    strMessage = "<h1>" + APP_NAME_AND_VERSION + "</h1>";
    strMessage = strMessage + "<small>Christian Aguilera, " + __DATE__ + " " + __TIME__ +
                 "</small><br /><br />";
    const QString strQt =
        tr("Built with Qt %1 (running Qt %2).").arg(QT_VERSION_STR).arg(qVersion());
    strMessage = strMessage + "<small>" + strQt + "</small><br /><br /><br />";
    strMessage = strMessage + tr("%1 is a tool that monitors the system's clipboard to create "
                                 "clipboard bookmarks and to keep track of the clipboard history.")
                                  .arg(APP_NAME);

    QMessageBox messageBox(
        QMessageBox::NoIcon, tr("About %1").arg(APP_NAME), strMessage, QMessageBox::NoButton, this);
    messageBox.addButton(new QPushButton(tr("&Close"), &messageBox), QMessageBox::AcceptRole);
    const QIcon icon = QIcon(":/resources/clipboard.svg");
    messageBox.setIconPixmap(icon.pixmap(icon.actualSize(QSize(128, 128))));
    messageBox.exec();
}

void MainWindow::slt_shortcutsTriggered(bool bChecked /* = false*/)
{
    Q_UNUSED(bChecked);

    QString strMessage;
    strMessage = "<h1>" + tr("Shortcuts") + "</h1>";
    strMessage = strMessage + tr("Store into slot %1").arg(1) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+1</strong><br/>";
    strMessage = strMessage + tr("Store into slot %1").arg(2) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+2</strong><br/>";
    strMessage = strMessage + tr("Store into slot %1").arg(3) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+3</strong><br/>";
    strMessage = strMessage + tr("Store into slot %1").arg(4) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+4</strong><br />";
    strMessage = strMessage + tr("Store into slot %1").arg(5) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+5</strong><br />";
    strMessage = strMessage + tr("Store into slot %1").arg(6) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+6</strong><br />";
    strMessage = strMessage + tr("Store into slot %1").arg(7) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+7</strong><br />";
    strMessage = strMessage + tr("Store into slot %1").arg(8) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+8</strong><br />";
    strMessage = strMessage + tr("Store into slot %1").arg(9) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+9</strong><br /><br />";
    strMessage = strMessage + tr("Retrieve from slot %1").arg(1) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+F1</strong><br />";
    strMessage = strMessage + tr("Retrieve from slot %1").arg(2) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+F2</strong><br />";
    strMessage = strMessage + tr("Retrieve from slot %1").arg(3) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+F3</strong><br />";
    strMessage = strMessage + tr("Retrieve from slot %1").arg(4) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+F4</strong><br />";
    strMessage = strMessage + tr("Retrieve from slot %1").arg(5) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+F5</strong><br />";
    strMessage = strMessage + tr("Retrieve from slot %1").arg(6) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+F6</strong><br />";
    strMessage = strMessage + tr("Retrieve from slot %1").arg(7) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+F7</strong><br />";
    strMessage = strMessage + tr("Retrieve from slot %1").arg(8) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+F8</strong><br />";
    strMessage = strMessage + tr("Retrieve from slot %1").arg(9) +
                 ": &nbsp;&nbsp;<strong>Super+Alt+F9</strong><br /><br />";
    strMessage = strMessage + tr("Navigate history") +
                 ": &nbsp;&nbsp;<strong>Super+Alt+Mouse "
                 "Wheel</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br "
                 "/><br />";
    strMessage =
        strMessage + tr("Toggle clipboard") + ": &nbsp;&nbsp;<strong>Super+Alt+Q</strong><br />";
    strMessage = strMessage + "<br /><br />";

    QMessageBox messageBox(
        QMessageBox::NoIcon, tr("Shortcuts"), strMessage, QMessageBox::NoButton, this);
    messageBox.addButton(new QPushButton(tr("&Close"), &messageBox), QMessageBox::AcceptRole);
    const QIcon icon = QIcon(":/resources/shortcuts.svg");
    messageBox.setIconPixmap(icon.pixmap(icon.actualSize(QSize(64, 64))));
    messageBox.exec();
}

void MainWindow::slt_settingsTriggered(bool bChecked /* = false*/)
{
    Q_UNUSED(bChecked);

    SettingsDialog settingsDialog(this);
    settingsDialog.show();
    settingsDialog.exec();
}

void MainWindow::slt_showTriggered(bool bChecked)
{
    Q_UNUSED(bChecked);

    slt_raiseWindow();
}

void MainWindow::slt_hideTriggered(bool bChecked)
{
    Q_UNUSED(bChecked);

    slt_hideWindow();
}

void MainWindow::slt_raiseWindow()
{
    if (isMinimized())
    {
        setWindowState((windowState() & !Qt::WindowMinimized) | Qt::WindowActive);
    }
    show();
    raise();
    activateWindow();
}

void MainWindow::slt_hideWindow()
{
    hide();
}

void MainWindow::slt_clearFilter()
{
    d->m_pLineEdit->clear();
}

void MainWindow::slt_applicationStateChanged(Qt::ApplicationState state)
{
    d->m_nApplicationState = state;
    d->m_nApplicationStateTime = QDateTime::currentMSecsSinceEpoch();
}

void MainWindow::slt_showMessage(const QString& strTitle,
                                 const QString& strMessage,
                                 const QSystemTrayIcon::MessageIcon icon,
                                 const int nTimeoutMs)
{
    if (SETTING("Behaviour/show_popups", true).toBool())
    {
        d->m_systemTrayIcon.showMessage(strTitle, strMessage, icon, nTimeoutMs);
    }

    if (SETTING("Behaviour/show_notifications", true).toBool())
    {
        Notification::showMessage(strTitle, strMessage, nTimeoutMs);
    }
}

void MainWindow::slt_systemTrayIconExcited()
{
    static const QIcon icExcited(":/resources/clipboard_excited.svg");
    ::setSystemTrayIcon(d->m_systemTrayIcon, icExcited);

    QTimer::singleShot(1500, this, SLOT(slt_systemTrayIconIdle()));
}

void MainWindow::slt_systemTrayIconDisabledExcited(const bool blink)
{
    static const QIcon icDisabledExcited(":/resources/clipboard_disabled_excited.svg");
    ::setSystemTrayIcon(d->m_systemTrayIcon, icDisabledExcited);

    if (blink)
    {
        QTimer::singleShot(300, this, SLOT(slt_systemTrayIconIdle()));
        QTimer::singleShot(600, this, SLOT(slt_systemTrayIconDisabledExcited()));
        QTimer::singleShot(900, this, SLOT(slt_systemTrayIconIdle()));
        QTimer::singleShot(1200, this, SLOT(slt_systemTrayIconDisabledExcited()));
        QTimer::singleShot(1500, this, SLOT(slt_systemTrayIconIdle()));
    }
}

void MainWindow::slt_systemTrayIconToggled()
{
    static const QIcon icToggled(":/resources/clipboard_toggled.svg");
    ::setSystemTrayIcon(d->m_systemTrayIcon, icToggled);

    QTimer::singleShot(1500, this, SLOT(slt_systemTrayIconIdle()));
}

void MainWindow::slt_systemTrayIconIdle()
{
    const bool bEnabled = ClipboardManager::instance().isEnabled();

    static const QIcon icDisabled(":/resources/clipboard_disabled.svg");
    ::setSystemTrayIcon(d->m_systemTrayIcon, bEnabled ? windowIcon() : icDisabled);
}
