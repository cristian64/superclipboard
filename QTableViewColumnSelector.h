#ifndef QTABLEVIEWCOLUMNSELECTOR_H
#define QTABLEVIEWCOLUMNSELECTOR_H

#include <QtCore/QObject>
#include <QtWidgets/QTableView>

class QTableViewColumnSelector final : QObject
{
    Q_OBJECT

public:
    QTableViewColumnSelector();
    ~QTableViewColumnSelector();
    void setTableView(QTableView* pTableView);

protected slots:
    void slt_customContextMenuRequested(const QPoint& ptPos);

private:
    Q_DISABLE_COPY(QTableViewColumnSelector)
    QTableView* m_pTableView;
};

#endif  // QTABLEVIEWCOLUMNSELECTOR_H
