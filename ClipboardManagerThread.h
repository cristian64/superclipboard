#ifndef CLIPBOARDMANAGERTHREAD_H
#define CLIPBOARDMANAGERTHREAD_H

#include <memory>

#include <QtCore/QThread>

class ClipboardManagerThread final : public QThread
{
    Q_OBJECT

public:
    enum ActivationMode
    {
        ACTIVATION_MODE_STORE,
        ACTIVATION_MODE_RETRIEVE
    };

    ClipboardManagerThread();
    ~ClipboardManagerThread();

signals:

    void sig_slotActivated(const quint8 nSlot, const qint32 eActivationMode);
    void sig_toggleClipboard();
    void sig_scrollUp();
    void sig_scrollDown();

protected:
    void run() override;
    bool event(QEvent* pEvent) override;

private:
    Q_DISABLE_COPY(ClipboardManagerThread)

    class PrivateData;
    std::unique_ptr<PrivateData> d;
};

#endif  // CLIPBOARDMANAGERTHREAD_H
