#include "SettingsDialog.h"

#include <QtWidgets/QFileDialog>

#include "Settings.h"
#include "ui_SettingsDialog.h"

SettingsDialog::SettingsDialog(MainWindow* parent)
    : QDialog(parent), ui(new Ui::SettingsDialog), m_pMainWindow(parent)
{
    ui->setupUi(this);

    restoreGeometry(SETTING("Layout/settings_geometry").toByteArray());

    ui->labelSettingsLocationPath->setText(SETTINGS.fileName());
    ui->lineEditDataLocation->setText(SETTING("data_dir_path", "").toString());
    ui->spinBoxMaxHistoryCount->setValue(SETTING("max_history_count", 1000).toInt());

    {
        const QStringList strlstPatternSubstitutions{
            SETTING("pattern_substitutions").toStringList()};
        QStringList strlstPatterns;
        QStringList strlstSubstitutions;
        for (qsizetype i{0}; i < strlstPatternSubstitutions.size() / 2 * 2; i += 2)
        {
            strlstPatterns << strlstPatternSubstitutions[i];
            strlstSubstitutions << strlstPatternSubstitutions[i + 1];
        }
        ui->plainTextEditPatterns->setPlainText(strlstPatterns.join("\n"));
        ui->plainTextEditSubstitutions->setPlainText(strlstSubstitutions.join("\n"));
    }

    ui->checkBoxShowPopups->setChecked(SETTING("Behaviour/show_popups", false).toBool());
    ui->checkBoxShowNotifications->setChecked(
        SETTING("Behaviour/show_notifications", true).toBool());
    ui->checkBoxShowSystemTrayIcon->setChecked(
        SETTING("Layout/system_tray_icon_visible", true).toBool());
    ui->checkBoxConfirmDeletion->setChecked(SETTING("Behaviour/confirm_deletion", true).toBool());
}

SettingsDialog::~SettingsDialog()
{
    {
        QStringList strlstPatterns{ui->plainTextEditPatterns->toPlainText().split("\n")};
        QStringList strlstSubstitutions{ui->plainTextEditSubstitutions->toPlainText().split("\n")};
        while (strlstPatterns.size() < strlstSubstitutions.size())
        {
            strlstPatterns << "";
        }
        while (strlstSubstitutions.size() < strlstPatterns.size())
        {
            strlstSubstitutions << "";
        }

        QStringList strlstPatternSubstitutions;
        for (qsizetype i{0}; i < strlstPatterns.size(); ++i)
        {
            strlstPatternSubstitutions << strlstPatterns[i] << strlstSubstitutions[i];
        }

        SET_SETTING("pattern_substitutions", strlstPatternSubstitutions);
    }

    SET_SETTING("Layout/settings_geometry", saveGeometry());

    delete ui;
}

void SettingsDialog::on_spinBoxMaxHistoryCount_valueChanged(int nValue)
{
    SET_SETTING("max_history_count", nValue);
}

void SettingsDialog::on_checkBoxConfirmDeletion_toggled(bool bChecked)
{
    SET_SETTING("Behaviour/confirm_deletion", bChecked);
}

void SettingsDialog::on_checkBoxShowPopups_toggled(bool bChecked)
{
    SET_SETTING("Behaviour/show_popups", bChecked);
}

void SettingsDialog::on_checkBoxShowNotifications_toggled(bool bChecked)
{
    SET_SETTING("Behaviour/show_notifications", bChecked);
}

void SettingsDialog::on_checkBoxShowSystemTrayIcon_toggled(bool bChecked)
{
    SET_SETTING("Layout/system_tray_icon_visible", bChecked);
    if (m_pMainWindow)
    {
        m_pMainWindow->getSystemTrayIconRef().setVisible(bChecked);
    }
}

void SettingsDialog::on_pushButtonClose_clicked()
{
    close();
}

void SettingsDialog::on_pushButtonBrowse_clicked()
{
    const QString strDirPath =
        QFileDialog::getExistingDirectory(this, QString(), SETTING("data_dir_path", "").toString());
    if (strDirPath.size())
    {
        ui->lineEditDataLocation->setText(strDirPath);
        SET_SETTING("data_dir_path", strDirPath);
    }
}

void SettingsDialog::on_lineEditDataLocation_editingFinished()
{
    SET_SETTING("data_dir_path", ui->lineEditDataLocation->text());
}
