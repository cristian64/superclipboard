#ifndef CLIPBOARDMODEL_H
#define CLIPBOARDMODEL_H

#include <memory>

#include <QtCore/QAbstractItemModel>
#include <QtCore/QSortFilterProxyModel>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QSystemTrayIcon>

class ClipboardItem;

class ClipboardModel final : public QAbstractItemModel
{
    Q_OBJECT

public:
    enum ClipboardModelType
    {
        CLIPBOARD_INVALID,
        CLIPBOARD_HISTORY,       // Bounded to a large value
        CLIPBOARD_SLOT_HISTORY,  // Not bounded
        CLIPBOARD_SLOTS          // Bounded to just 9 slots
    };

    enum Columns
    {
        COLUMN_TIME,
        COLUMN_TAG,
        COLUMN_SIZE,
        COLUMN_PREVIEW,
        COLUMN_COUNT,
        COLUMN_FIRST = COLUMN_TIME,
    };

    explicit ClipboardModel(const ClipboardModelType eClipboardModelType,
                            QObject* pParent = nullptr);
    ~ClipboardModel();

    QModelIndex index(int row, int column, const QModelIndex& parent) const;
    QModelIndex parent(const QModelIndex& child) const;
    int rowCount(const QModelIndex& parent) const;
    int columnCount(const QModelIndex& parent) const;
    QVariant data(const QModelIndex& index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QHeaderView::ResizeMode getResizeMode(int section, Qt::Orientation orientation) const;

    void showEditTagDialog(const quint32 nIndex);
    void showCustomContextMenu(const QList<quint32>& nlstIndexes);
    const ClipboardItem* getClipboardItem(const quint32 nIndex, const bool bFullLoad = true) const;
    const QString& getFilter() const;

signals:
    void sig_showMessage(const QString& title,
                         const QString& message,
                         QSystemTrayIcon::MessageIcon icon = QSystemTrayIcon::Information,
                         int millisecondsTimeoutHint = 10000);

public slots:
    void slt_filterChanged(const QString& strFilter);
    void slt_slotRequested(const quint8 nSlot);
    void slt_clipboardRequested(const quint32 nIndex);
    void slt_clipboardDelete(const QList<quint32>& nlstIndexes);
    void slt_toggleClipboard();
    void slt_scrollUp();
    void slt_scrollDown();

    void slt_fileRemoved(const QString& strFilePath);
    void slt_fileAdded(const QString& strFilePath);

private:
    qint32 findIndexByFilePath(const QString& strFilePath) const;
    qint32 getSlotIndexByFilePath(const QString& strFilePath) const;
    QString cookFilePath(const ClipboardItem* const pItem, const quint8 nSlot) const;
    QString getFileExtension() const;
    void scroll(int nDirection);

    Q_DISABLE_COPY(ClipboardModel)

    class PrivateData;
    std::unique_ptr<PrivateData> d;
};

class ClipboardSortFilterProxyModel final : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    ClipboardSortFilterProxyModel(QObject* parent = nullptr);
    bool filterAcceptsRow(int row, const QModelIndex& parent) const override;

    Q_DISABLE_COPY(ClipboardSortFilterProxyModel)
};

#endif  // CLIPBOARDMODEL_H
