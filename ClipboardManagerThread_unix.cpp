// clang-format off
#include "ClipboardManagerThread.h"
// clang-format on

#include <X11/Xlib.h>

#define KEY_1 10     // Keycode for [1] key.
#define KEY_F1 67    // Keycode for [F1] key.
#define KEY_COUNT 9  // Handling from [1] to [9] (and from [F1] to [F9]).
#define KEY_Q 24     // Keycode for [Q] key.

class ClipboardManagerThread::PrivateData final
{
public:
    PrivateData() : m_pDisplay(nullptr), m_wnRoot(0), m_bCancelled(false) {}

    ~PrivateData()
    {
        Q_ASSERT(m_pDisplay == nullptr);
        Q_ASSERT(m_wnRoot == 0);
    }

    Display* m_pDisplay;
    Window m_wnRoot;
    QAtomicInt m_bCancelled;

private:
    Q_DISABLE_COPY(PrivateData)
};

ClipboardManagerThread::ClipboardManagerThread() : d(new PrivateData)
{
    moveToThread(this);

    start();
}

ClipboardManagerThread::~ClipboardManagerThread()
{
    // Cancel the loop.
    d->m_bCancelled = true;

    // Send dummy expose event to wake the thread up.
    XExposeEvent exposeEvent;
    exposeEvent.type = Expose;
    exposeEvent.serial = 0;
    exposeEvent.send_event = true;
    exposeEvent.display = d->m_pDisplay;
    exposeEvent.window = d->m_wnRoot;
    exposeEvent.x = 0;
    exposeEvent.y = 0;
    exposeEvent.width = 0;
    exposeEvent.height = 0;
    exposeEvent.count = 0;
    XSendEvent(
        d->m_pDisplay, d->m_wnRoot, 0, ExposureMask, reinterpret_cast<XEvent*>(&exposeEvent));
    XFlush(d->m_pDisplay);

    quit();

    // Wait until finished.
    if (!wait(5000))
    {
        Q_ASSERT_X(0, Q_FUNC_INFO, "Thread shutdown timed out");
        terminate();
    }

    d.reset();
}

bool ClipboardManagerThread::event(QEvent* pEvent)
{
    return QThread::event(pEvent);
}

void ClipboardManagerThread::run()
{
    // Open the display and get the default window.
    d->m_pDisplay = XOpenDisplay(nullptr);
    Q_ASSERT(d->m_pDisplay);
    if (!d->m_pDisplay)
    {
        return;
    }

    d->m_wnRoot = DefaultRootWindow(d->m_pDisplay);
    Q_ASSERT(d->m_wnRoot);

    // Select expose events. Used to wake XNextEvent up.
    if (!XSelectInput(d->m_pDisplay, d->m_wnRoot, ExposureMask))
    {
        // Close and clear.
        XCloseDisplay(d->m_pDisplay);
        d->m_pDisplay = nullptr;
        d->m_wnRoot = 0;
        return;
    }

    constexpr auto SuperMask = Mod4Mask;
    constexpr auto AltMask = Mod1Mask;

    // Basically we want any mask that contains the Ctrl and Shift flags.
    // This is so that this works even if, for example, Caps Lock is on, etc.
    for (int j = 0; j < 256; ++j)
    {
        if (!((j & (SuperMask | AltMask)) == (SuperMask | AltMask)))
        {
            continue;
        }

        const unsigned int eModifiers = static_cast<unsigned int>(SuperMask | AltMask | j);

        // Subscribe to the key events (numbers and Fs keys),
        // combined with the Ctrl and Shift modifiers.
        for (int i = 0; i < KEY_COUNT; ++i)
        {
            XGrabKey(d->m_pDisplay,
                     KEY_1 + i,
                     eModifiers,
                     d->m_wnRoot,
                     False,
                     GrabModeAsync,
                     GrabModeAsync);
            XGrabKey(d->m_pDisplay,
                     KEY_F1 + i,
                     eModifiers,
                     d->m_wnRoot,
                     False,
                     GrabModeAsync,
                     GrabModeAsync);
        }

        // For toggling the last two clipboads.
        XGrabKey(
            d->m_pDisplay, KEY_Q, eModifiers, d->m_wnRoot, False, GrabModeAsync, GrabModeAsync);

        // For mouse wheel.
        XGrabButton(d->m_pDisplay,
                    Button4,
                    eModifiers,
                    d->m_wnRoot,
                    False,
                    ButtonPressMask,
                    GrabModeAsync,
                    GrabModeAsync,
                    None,
                    None);
        XGrabButton(d->m_pDisplay,
                    Button5,
                    eModifiers,
                    d->m_wnRoot,
                    False,
                    ButtonPressMask,
                    GrabModeAsync,
                    GrabModeAsync,
                    None,
                    None);
    }

    // Loop until cancelled.
    while (!d->m_bCancelled)
    {
        // Wait for the next event.
        XEvent ev;
        memset(&ev, 0, sizeof(ev));
        XNextEvent(d->m_pDisplay, &ev);

        if (ev.type == KeyPress)
        {
            const XKeyPressedEvent& evKeyPressed = *(XKeyPressedEvent*)&ev;
            const unsigned int nKeycode = evKeyPressed.keycode;

            if (KEY_1 <= nKeycode && nKeycode < KEY_1 + KEY_COUNT)
            {
                emit sig_slotActivated(static_cast<uint8_t>(nKeycode - KEY_1 + 1),
                                       ACTIVATION_MODE_STORE);
            }
            else if (KEY_F1 <= nKeycode && nKeycode <= KEY_F1 + KEY_COUNT)
            {
                emit sig_slotActivated(static_cast<uint8_t>(nKeycode - KEY_F1 + 1),
                                       ACTIVATION_MODE_RETRIEVE);
            }
            else if (nKeycode == KEY_Q)
            {
                emit sig_toggleClipboard();
            }
        }

        if (ev.type == ButtonPress)
        {
            if (ev.xbutton.button == Button4)
            {
                emit sig_scrollUp();
            }
            else if (ev.xbutton.button == Button5)
            {
                emit sig_scrollDown();
            }
        }
    }

    // Unsubscribe from key event notifications.
    // This may not be really needed, but let's do it regardless.
    for (int j = 0; j < 256; ++j)
    {
        if (!((j & (SuperMask | AltMask)) == (SuperMask | AltMask)))
        {
            continue;
        }

        const unsigned int eModifiers = static_cast<unsigned int>(SuperMask | AltMask | j);

        // Subscribe to the key events (numbers and Fs keys),
        // combined with the Ctrl and Shift modifiers.
        for (int i = 0; i < KEY_COUNT; ++i)
        {
            XUngrabKey(d->m_pDisplay, KEY_1 + i, eModifiers, d->m_wnRoot);
            XUngrabKey(d->m_pDisplay, KEY_F1 + i, eModifiers, d->m_wnRoot);
        }

        // For toggling the last two clipboads.
        XUngrabKey(d->m_pDisplay, KEY_Q, eModifiers, d->m_wnRoot);

        // For mouse wheel.
        XUngrabButton(d->m_pDisplay, Button4, eModifiers, d->m_wnRoot);
        XUngrabButton(d->m_pDisplay, Button5, eModifiers, d->m_wnRoot);
    }

    // Close and clear.
    XCloseDisplay(d->m_pDisplay);
    d->m_pDisplay = nullptr;
    d->m_wnRoot = 0;
}
