#ifndef NOTIFICATION_H
#define NOTIFICATION_H

#include <QtCore/QString>
#include <QtWidgets/QWidget>

namespace Ui
{
class Notification;
}

class Notification final : public QWidget
{
public:
    Notification();
    ~Notification();

    static void showMessage(const QString& strTitle, const QString& strMessage, int nTimeoutMs);

    Q_DISABLE_COPY(Notification)

protected:
    void mousePressEvent(QMouseEvent* event) override;

    void updateText(const QString& strTitle, const QString& strMessage);
    void autoAdjustSize();

private:
    Ui::Notification* ui;
};

#endif  // NOTIFICATION_H
