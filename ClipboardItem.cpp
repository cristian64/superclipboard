#include "ClipboardItem.h"

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QStandardPaths>
#include <QtCore/QTemporaryDir>
#include <QtCore/QUrl>
#include <QtGui/QImage>

#include "ClipboardCache.h"
#include "ClipboardDataStream.h"
#include "QMimeDataEx.h"

ClipboardItem::ClipboardItem() : m_nId(0), m_nSize(0), m_bFullLoad(false) {}

ClipboardItem::ClipboardItem(const QMimeData& mimeData,
                             const QDateTime& dtTime /* = QDateTime::currentDateTimeUtc()*/,
                             const QString& strTag /* = Utils::getHostName()*/
                             )
    : m_dtTime(dtTime),
      m_nId(m_dtTime.toMSecsSinceEpoch()),
      m_strTag(strTag),
      m_nSize(0),
      m_bFullLoad(true)
{
    // Since there isn't a copy constructor defined,
    // we have to copy manually.
    QMimeDataEx::copyMimeData(mimeData, m_mimeData);
    m_nSize = QMimeDataEx::calculateSize(m_mimeData);

    if (mimeData.hasText())
    {
        m_strPreview = mimeData.text();
    }
    else if (mimeData.hasHtml())
    {
        m_strPreview = mimeData.html();
    }
    else if (mimeData.hasUrls())
    {
        const QList<QUrl> lstUrls = mimeData.urls();
        QStringList strlstUrls;
        for (const QUrl& url : lstUrls)
        {
            strlstUrls << url.toString();
        }

        m_strPreview = strlstUrls.join(", ");
    }
    else if (mimeData.hasImage())
    {
        QImage image;
        QMimeDataEx::getImage(mimeData, image);
        const int nWidth = image.width();
        const int nHeight = image.height();
        if (nWidth && nHeight)
        {
            m_strPreview = "[ img | " + QString("%1x%2").arg(nWidth).arg(nHeight) + " ]";
        }
        else
        {
            m_strPreview = "[ img ]";
        }
    }
}

bool ClipboardItem::loadFromFile(const QString& strFilePath, const bool bFullLoad)
{
    // Check the cache first. Opening the file is prohibitive compared to
    // checking the cache.
    if (!bFullLoad)
    {
        if (ClipboardCache::instance().loadFromCache(strFilePath, *this))
        {
            return true;
        }
    }

    QFile file(strFilePath);
    if (file.open(QIODevice::ReadOnly))
    {
        ClipboardDataStream stream(&file);

        bool bReserved = false;

        stream >> m_nId >> m_dtTime >> m_strTag >> m_nSize >> m_strPreview >> bReserved;

        // The ID must match the date time.
        // Otherwise we'll assume wrong data.
        if (m_nId != m_dtTime.toMSecsSinceEpoch())
        {
            return false;
        }

        if (bFullLoad)
        {
            stream >> m_mimeData;
        }
        m_bFullLoad = bFullLoad;

        ClipboardCache::instance().saveToCache(strFilePath, *this);

        Q_UNUSED(bReserved);

        return true;
    }

    return false;
}

bool ClipboardItem::saveToFile(const QString& strFilePath, const bool bUpdateCache) const
{
    static const QTemporaryDir s_temporaryDir(
        QStandardPaths::writableLocation(QStandardPaths::TempLocation) + "/superclipboard_XXXXXX");

    const QFileInfo fiFilePath{strFilePath};
    const QString strTempFilePath{s_temporaryDir.filePath(fiFilePath.fileName())};

    {
        QFile file(strTempFilePath);
        if (!file.open(QIODevice::WriteOnly))
            return false;

        ClipboardDataStream stream(&file);

        stream << m_nId << m_dtTime << m_strTag << m_nSize << m_strPreview
               << false  // Reserved for some extra metadata in the future (such
                         // as thumbnail)
               << m_mimeData;
    }

    QDir().mkpath(fiFilePath.absolutePath());

    // Move to destination atomically.
    QFile(strFilePath).remove();
    QFile::rename(strTempFilePath, strFilePath);

    // Update entry in the cache.
    if (bUpdateCache)
    {
        ClipboardCache::instance().saveToCache(strFilePath, *this);
    }

    return true;
}

bool ClipboardItem::needsHighlighting(const QString& strFilter) const
{
    if (strFilter.isEmpty())
    {
        return false;
    }
    return m_strTag.contains(strFilter, Qt::CaseInsensitive) ||
           m_strPreview.contains(strFilter, Qt::CaseInsensitive);
}
