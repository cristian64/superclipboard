#ifndef CLIPBOARDITEM_H
#define CLIPBOARDITEM_H

#include <QtCore/QDateTime>
#include <QtCore/QMimeData>

#include "Utils.h"

class ClipboardItem final
{
public:
    ClipboardItem();
    ClipboardItem(const QMimeData& mimeData,
                  const QDateTime& dtTime = QDateTime::currentDateTimeUtc(),
                  const QString& strTag = Utils::getHostName());

    bool loadFromFile(const QString& strFilePath, const bool bFullLoad);
    bool saveToFile(const QString& strFilePath, const bool bUpdateCache) const;
    bool needsHighlighting(const QString& strFilter) const;

    QMimeData m_mimeData;
    QDateTime m_dtTime;
    qint64 m_nId;
    QString m_strTag;
    quint64 m_nSize;
    QString m_strPreview;

    bool m_bFullLoad;

private:
    Q_DISABLE_COPY(ClipboardItem)
};

#endif  // CLIPBOARDITEM_H
