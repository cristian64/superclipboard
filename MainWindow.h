#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <memory>

#include <QtWidgets/QMainWindow>
#include <QtWidgets/QSystemTrayIcon>

#include "ClipboardManager.h"
#include "ClipboardModel.h"

class MainWindow final : public QMainWindow
{
    Q_OBJECT

public:
    static const int c_nRestartExitCode{0x12E57412};  // "Restart" in hexspeak.

    static MainWindow& instance();

    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

    QSystemTrayIcon& getSystemTrayIconRef();

    void blinkDisabledSystemTrayIcon();

protected:
    void saveLayout() const;
    void restoreLayout();
    void closeEvent(QCloseEvent* event);
    bool eventFilter(QObject* pWatched, QEvent* pEvent);

    void showPreview(const ClipboardModel* const pModel, const quint32 nIndex);

protected slots:
    void slt_systemTrayIconActivated(QSystemTrayIcon::ActivationReason eActivationReason);
    void slt_systemTrayIconExcited();
    void slt_systemTrayIconDisabledExcited(bool blink = false);
    void slt_systemTrayIconToggled();
    void slt_systemTrayIconIdle();
    void slt_customContextMenuRequested(const QPoint& ptPos);
    void slt_currentRowChanged(const QModelIndex& current, const QModelIndex& previous);
    void slt_currentColumnChanged(const QModelIndex& current, const QModelIndex& previous);
    void slt_doubleClicked(const QModelIndex& modelIndex);
    void slt_clicked(const QModelIndex& index);
    void slt_restartTriggered(bool bChecked = false);
    void slt_exitTriggered(bool bChecked = false);
    void slt_enableToggled(bool bChecked = false);
    void slt_aboutTriggered(bool bChecked = false);
    void slt_shortcutsTriggered(bool bChecked = false);
    void slt_settingsTriggered(bool bChecked = false);
    void slt_showTriggered(bool bChecked = false);
    void slt_hideTriggered(bool bChecked = false);
    void slt_raiseWindow();
    void slt_hideWindow();
    void slt_clearFilter();
    void slt_applicationStateChanged(Qt::ApplicationState state);
    void slt_showMessage(const QString& strTitle,
                         const QString& strMessage,
                         QSystemTrayIcon::MessageIcon icon,
                         int nTimeoutMs);

private:
    Q_DISABLE_COPY(MainWindow)

    class PrivateData;
    std::unique_ptr<PrivateData> d;
};

#endif  // MAINWINDOW_H
