#ifndef CLIPBOARDDATASTREAM_H
#define CLIPBOARDDATASTREAM_H

#include <QtCore/QDataStream>
#include <QtCore/QIODevice>

class ClipboardDataStream : public QDataStream
{
public:
    ClipboardDataStream(QIODevice* const d) : QDataStream(d)
    {
        // It is possible this version won't need to change ever. If it was to change, we'll need to
        // find ways to determine which files are generated with the new version. In some places, a
        // version number was added to the stream, and that might be enough to determine whether a
        // different algorithm is needed for reading the file... Perhaps adding a magic number at
        // the very beginning of new files is enough for detecting early that the binary data is to
        // be read differently. At this point I'm unsure if Qt's QDataStream adds any payload to
        // primitive types such as quint64 (or if it ever will). We might need to append the magic
        // number manually to the byte array, to ensure the magic number appears in the very 8 bytes
        // of the file.
        setVersion(QDataStream::Qt_5_6);
    }

private:
    Q_DISABLE_COPY(ClipboardDataStream)
};

#endif  // CLIPBOARDDATASTREAM_H
