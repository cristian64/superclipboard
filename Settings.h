#ifndef SETTINGS_H
#define SETTINGS_H

#include <QtCore/QSettings>

#define SETTINGS Settings::instance()
#define SETTING Settings::instance().getValue
#define SET_SETTING Settings::instance().setValue
#define SETTINGS_GROUP(x) SettingsScopedGroup ssg(Settings::instance(), x)

class Settings final : public QSettings
{
public:
    static Settings& instance();

    using QSettings::QSettings;

    QVariant getValue(const QString& strKey, const QVariant& varDefaultValue = QVariant());

    Q_DISABLE_COPY(Settings)

protected:
    QVariant value(const QString& key, const QVariant& defaultValue = QVariant()) const;
};

class SettingsScopedGroup final
{
public:
    SettingsScopedGroup(Settings& settings, const QString& strGroup) : m_settings(settings)
    {
        settings.beginGroup(strGroup);
    }

    ~SettingsScopedGroup() { m_settings.endGroup(); }

private:
    Q_DISABLE_COPY(SettingsScopedGroup)

    Settings& m_settings;
};

#endif  // SETTINGS_H
