#include "SingleApplication.h"

#include <QtCore/QDir>
#include <QtCore/QFileInfo>

SingleApplication::SingleApplication(const QString& strFilePath)
{
    // Make the path.
    const QFileInfo fiFile(strFilePath);
    QDir().mkdir(fiFile.absoluteDir().absolutePath());

    // Sometimes it may fail just due to wrong permissions.
    const QFile::Permissions nPermissions =
        QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ExeOwner |
        QFileDevice::ReadUser | QFileDevice::WriteUser | QFileDevice::ExeUser |
        QFileDevice::ReadGroup | QFileDevice::WriteGroup | QFileDevice::ExeGroup |
        QFileDevice::ReadOther | QFileDevice::WriteOther | QFileDevice::ExeOther;
    QFile::setPermissions(strFilePath, nPermissions);

    m_apFile.reset(new QLockFile(strFilePath));
    m_apFile->tryLock(200);
}

SingleApplication::~SingleApplication()
{
    m_apFile->unlock();
}

bool SingleApplication::isOwner() const
{
    return m_apFile->isLocked();
}
