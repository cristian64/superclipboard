#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QFileInfo>
#include <QtCore/QLibraryInfo>
#include <QtCore/QLocale>
#include <QtCore/QTimeZone>
#include <QtCore/QTranslator>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStyleFactory>

#include "ClipboardItem.h"
#include "ClipboardManager.h"
#include "DirectoryMonitor.h"
#include "Globals.h"
#include "MainWindow.h"
#include "Settings.h"
#include "SingleApplication.h"

int run(int argc, char* argv[])
{
    QApplication app(argc, argv);

    // Prevent application from ending when a dialog is closed.
    app.setQuitOnLastWindowClosed(false);

    // If a filepath is given, we just want to copy the clipboard
    // into the location.
    if (argc > 1)
    {
        const QString strFilePath = argv[1];
        QDateTime dtTime = QDateTime::currentDateTimeUtc();
        if (argc > 2)
        {
            // Attempt to parse date.
            const qlonglong nMSecs = QString(argv[2]).toLongLong();
            dtTime = QDateTime::fromMSecsSinceEpoch(nMSecs, QTimeZone::utc());
        }
        ClipboardManager::readAndStoreClipboardContent(strFilePath, dtTime);

        return 0;
    }

    // Attempt to read the locale from an environment variable (for devs).
    const QByteArray s_baLocaleName = qgetenv("SUPERCLIPBOARD_LOCALE");

    // Install the translators.
    const QString strLocaleName = s_baLocaleName.size() ? s_baLocaleName : QLocale::system().name();

    const QString strLibraryPath =
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
        QLibraryInfo::path(QLibraryInfo::TranslationsPath);
#else
        QLibraryInfo::location(QLibraryInfo::TranslationsPath);
#endif

    QTranslator translatorQt;
    (void)translatorQt.load("qt_" + strLocaleName, strLibraryPath);
    app.installTranslator(&translatorQt);
    QTranslator translatorSuperClipboard;
    (void)translatorSuperClipboard.load("SuperClipboard_" + strLocaleName, ":/i18n");
    app.installTranslator(&translatorSuperClipboard);

    // Set a non-disgusting style.
    const QStringList strlstAvailableStyles = QStyleFactory::keys();
    const QStringList strlstPreferredStyles = QStringList() << "Fusion";
    for (const QString& strStyle : strlstPreferredStyles)
    {
        if (strlstAvailableStyles.contains(strStyle, Qt::CaseInsensitive))
        {
            app.setStyle(strStyle);
            break;
        }
    }

    // Single instance mode.
    const QString strDirPath = QFileInfo(SETTINGS.fileName()).absoluteDir().absolutePath();
    const QString strFilePath = strDirPath + "/SuperClipboard.lock";
    SingleApplication singleApplication(strFilePath);
    if (!singleApplication.isOwner())
    {
        const QString strTrMessage = QApplication::tr(
            "Another instance is already running. If you think this isn't the "
            "case, check the lock file's permissions: \"%1\"");
        const QString strMessage = strTrMessage.arg(strFilePath);

        qCritical() << strMessage;
        QMessageBox messageBox(QMessageBox::Icon::Warning, APP_NAME, strMessage);
        messageBox.addButton(new QPushButton(MainWindow::tr("&Close"), &messageBox),
                             QMessageBox::AcceptRole);
        messageBox.exec();
        return -1;
    }

    // Create the single instance here in the main thread.
    ClipboardManager clipboardManager;
    Q_UNUSED(clipboardManager);

    // Create the single instance here in the main thread.
    DirectoryMonitor directoryMonitor(Globals::getDataDirPath());
    Q_UNUSED(directoryMonitor);

    // Create and show the main window.
    MainWindow mainWindow;
    Q_UNUSED(mainWindow);

    return app.exec();
}

int main(const int argc, char* argv[])
{
    int nExitCode;

    do
    {
        nExitCode = run(argc, argv);

        if (nExitCode == MainWindow::c_nRestartExitCode)
            continue;

        break;
    } while (true);

    return nExitCode;
}
