#include "QMimeDataEx.h"

#include <QtCore/QIODevice>
#include <QtCore/QUrl>
#include <QtGui/QImage>

#include "ClipboardDataStream.h"

static constexpr quint8 c_nSerializerVersion{1};

QDataStream& operator<<(QDataStream& stream, const QMimeData& mimeData)
{
    // This is the version. In case we ever have to change this.
    stream << c_nSerializerVersion;

    // First write the formats.
    const QStringList strlstFormats = mimeData.formats();
    stream << static_cast<int>(strlstFormats.size());
    for (const QString& strFormat : strlstFormats)
    {
        stream << strFormat;
        stream << mimeData.data(strFormat);
    }

    // Then write the special formats.
    // Qt is all messed up, so we have to do this this way...
    if (mimeData.hasColor())
    {
        stream << true;
        stream << mimeData.colorData();
    }
    else
    {
        stream << false;
    }
    if (mimeData.hasHtml())
    {
        stream << true;
        stream << mimeData.html();
    }
    else
    {
        stream << false;
    }
    if (mimeData.hasImage())
    {
        stream << true;
        stream << mimeData.imageData();
    }
    else
    {
        stream << false;
    }
    if (mimeData.hasText())
    {
        stream << true;
        stream << mimeData.text();
    }
    else
    {
        stream << false;
    }
    if (mimeData.hasUrls())
    {
        stream << true;
        stream << mimeData.urls();
    }
    else
    {
        stream << false;
    }

    return stream;
}

QDataStream& operator>>(QDataStream& stream, QMimeData& mimeData)
{
    mimeData.clear();

    quint8 nVersion;
    stream >> nVersion;

    // A single version is supported at the moment, but we might need more in
    // time, specially if Qt breaks serialization of QVariant or other more
    // primitive types.
    if (nVersion != c_nSerializerVersion)
        return stream;

    int nFormats = 0;
    stream >> nFormats;
    for (int i = 0; i < nFormats; ++i)
    {
        QString strFormat;
        QByteArray baData;
        stream >> strFormat >> baData;

        mimeData.setData(strFormat, baData);
    }

    bool bHasColor = false;
    stream >> bHasColor;
    if (bHasColor)
    {
        QVariant varColorData;
        stream >> varColorData;
        mimeData.setColorData(varColorData);
    }

    bool bHasHtml = false;
    stream >> bHasHtml;
    if (bHasHtml)
    {
        QString strHtml;
        stream >> strHtml;
        mimeData.setHtml(strHtml);
    }

    bool bHasImage = false;
    stream >> bHasImage;
    if (bHasImage)
    {
        QVariant varImage;
        stream >> varImage;
        mimeData.setImageData(varImage);
    }

    bool bHasText = false;
    stream >> bHasText;
    if (bHasText)
    {
        QString strText;
        stream >> strText;
        mimeData.setText(strText);
    }

    bool bHasUrls = false;
    stream >> bHasUrls;
    if (bHasUrls)
    {
        QList<QUrl> lstUrls;
        stream >> lstUrls;
        mimeData.setUrls(lstUrls);
    }

    return stream;
}

bool operator==(const QMimeData& mimeData, const QMimeData& otherMimeData)
{
    // Same number of formats?
    const QStringList strlstFormats = mimeData.formats();
    const QStringList strlstOtherFormats = otherMimeData.formats();
    if (strlstFormats != strlstOtherFormats)
    {
        return false;
    }

    // Same formats?
    for (const QString& strFormat : strlstFormats)
    {
        const QByteArray baData = mimeData.data(strFormat);
        const QByteArray baOtherData = otherMimeData.data(strFormat);
        if (baData != baOtherData)
        {
            return false;
        }
    }

    // Same special data?
    // TODO: maybe not a big deal?

    return true;
}

namespace QMimeDataEx
{
QMimeData* cloneMimeData(const QMimeData& mimeData)
{
    QMimeData* pMimeData = new QMimeData();
    copyMimeData(mimeData, *pMimeData);
    return pMimeData;
}

void copyMimeData(const QMimeData& mimeDataSource, QMimeData& mimeDataDestination)
{
    // Copy all the available formats.
    const QStringList strlstFormats = mimeDataSource.formats();
    for (const QString& strFormat : strlstFormats)
    {
        const QByteArray baData = mimeDataSource.data(strFormat);
        mimeDataDestination.setData(strFormat, baData);
    }

    // Do the special formats separately. I believe
    // that Qt is all messed up to be honest, so let's do
    // this to be safe...
    if (mimeDataSource.hasColor())
    {
        mimeDataDestination.setColorData(mimeDataSource.colorData());
    }
    if (mimeDataSource.hasHtml())
    {
        mimeDataDestination.setHtml(mimeDataSource.html());
    }
    if (mimeDataSource.hasImage())
    {
        mimeDataDestination.setImageData(mimeDataSource.imageData());
    }
    if (mimeDataSource.hasText())
    {
        mimeDataDestination.setText(mimeDataSource.text());
    }
    if (mimeDataSource.hasUrls())
    {
        mimeDataDestination.setUrls(mimeDataSource.urls());
    }
}

bool hasData(const QMimeData& mimeData)
{
    // Good stuff?
    if (mimeData.hasColor() || mimeData.hasHtml() || mimeData.hasImage() || mimeData.hasText() ||
        mimeData.hasUrls())
    {
        return true;
    }

    // Or anything else?
    quint64 nSize = 0;
    const QStringList strlstFormats = mimeData.formats();
    for (const QString& strFormat : strlstFormats)
    {
        const QByteArray baData = mimeData.data(strFormat);
        nSize += static_cast<quint64>(baData.size());
    }
    return nSize;
}

namespace
{
// Just a silly class to use along with QDataStream
// to quickly measure how much data an object
// would require on disk.
class MeasuringDevice final : public QIODevice
{
public:
    MeasuringDevice() : m_nSize(0) { open(QIODevice::WriteOnly); }
    qint64 readData(char*, qint64) override { return 0; }
    qint64 writeData(const char*, qint64 len) override
    {
        m_nSize += static_cast<quint64>(len);
        return len;
    }
    quint64 getTotalWritten() const { return m_nSize; }

    Q_DISABLE_COPY(MeasuringDevice)

private:
    quint64 m_nSize;
};
}  // namespace

quint64 calculateSize(const QMimeData& mimeData)
{
    MeasuringDevice measuringDevice;
    ClipboardDataStream stream(&measuringDevice);
    stream << mimeData;

    return measuringDevice.getTotalWritten();
}

bool getImage(const QMimeData& mimeData, QImage& image)
{
    // This function had to be created as QMimeData::imageData is a bit shit.
    // Even though QMimeData::hasImage returns true, this function returns
    // a null image on occassions (at least happening with Linux, X11, Gnome2).

    // First, try the original function...
    image = qvariant_cast<QImage>(mimeData.imageData());
    if (image.width() && image.height())
    {
        return true;
    }

    // If it fails, try to parse an image from the MIME data.
    // Let's create a map to sort formats by length.
    // Then we can try to parse the image data in that order,
    // hoping that the larger, the better.
    QMultiMap<qint64, QString> mapFormats;
    const QStringList strlstFormats = mimeData.formats();
    for (const QString& strFormat : strlstFormats)
    {
        const QByteArray baData = mimeData.data(strFormat);
        const qint64 nSize = -qint64(qAbs(baData.size()));
        mapFormats.insert(nSize, strFormat);
    }

    // Attempt to load each format and pray for the best.
    for (const QString& strFormat : mapFormats)
    {
        image.loadFromData(mimeData.data(strFormat));
        if (image.width() && image.height())
        {
            return true;
        }
    }

    return false;
}

}  // namespace QMimeDataEx
