#include "Globals.h"

#include <QtCore/QDir>
#include <QtCore/QFileInfo>

#include "Settings.h"

namespace Globals
{
QString getDataDirPath()
{
    const QString strSettingsFilePath = SETTINGS.fileName();
    const QString strSettingsDirPath = QFileInfo(strSettingsFilePath).absolutePath();
    const QString strDefaultDataPath = QDir::toNativeSeparators(strSettingsDirPath + "/data");
    const QString strDataDirPath =
        QDir::toNativeSeparators(SETTING("data_dir_path", strDefaultDataPath).toString() + "/");
    return strDataDirPath;
}

}  // namespace Globals
