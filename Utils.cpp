#include "Utils.h"

#include <QtCore/QDir>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtNetwork/QHostInfo>

namespace Utils
{
QString cleanPath(const QString& strPath)
{
    return QDir::toNativeSeparators(QDir::cleanPath(strPath));
}

QString getHostName()
{
    return QHostInfo::localHostName();
}

QString convertToHumanReadableSize(const quint64 nBytes)
{
    double dBytes = static_cast<double>(nBytes);

    const QStringList strlstUnits = QStringList() << "B"
                                                  << "KiB"
                                                  << "MiB"
                                                  << "GiB"
                                                  << "TiB";
    for (const QString& strUnit : strlstUnits)
    {
        if (dBytes < 1000)
        {
            return QString("%1 %2").arg(QString::number(dBytes, 'f', 2)).arg(strUnit);
        }

        dBytes /= 1024;
    }

    return QString("%1 TiB").arg(QString::number(dBytes, 'f', 2));
}

bool isValidUrl(const QString& strUrl)
{
    // Apparently, getting this right is complicated.
    // First, let's see if Qt says this is valid.
    const QUrl url(strUrl);
    if (!url.isValid())
    {
        return false;
    }

    // Then confirm we have a dot somewhere.
    return !strUrl.startsWith(".") && strUrl.contains(".") && !strUrl.endsWith(".");
}

}  // namespace Utils
