#ifndef CLIPBOARDCACHE_H
#define CLIPBOARDCACHE_H

#include <string>
#include <unordered_map>

#include <QtCore/QDateTime>
#include <QtCore/QMimeData>
#include <QtCore/QString>

class ClipboardItem;

class ClipboardCache final
{
public:
    static ClipboardCache& instance();

    ClipboardCache();
    ~ClipboardCache();

    void purgeCache();

    bool loadFromCache(const QString& strFilePath, ClipboardItem& item);
    void saveToCache(const QString& strFilePath, const ClipboardItem& item);
    void removeFromCache(const QString& strFilePath);

private:
    Q_DISABLE_COPY(ClipboardCache)

    struct CachedItem final
    {
        QDateTime m_dtTime;
        qint64 m_nId{0};
        QString m_strTag;
        quint64 m_nSize{0};
        QString m_strPreview;
        bool m_bAccessed{false};
    };

    std::unordered_map<std::string, CachedItem> m_mItems;
};

#endif  // CLIPBOARDCACHE_H
