#include "DirectoryMonitor.h"

#include <QtCore/QDir>
#include <efsw/efsw.hpp>

#include "Utils.h"

static DirectoryMonitor* g_pInstance = nullptr;

class FileWatchListener final : public efsw::FileWatchListener
{
public:
    void handleFileAction(const efsw::WatchID watchid,
                          const std::string& dir,
                          const std::string& filename,
                          const efsw::Action action,
                          const std::string oldFilename) override
    {
        (void)watchid;
        (void)oldFilename;

        switch (action)
        {
        case efsw::Actions::Add:
            emit g_pInstance->sig_fileAdded(QString::fromStdString(dir + filename));
            break;
        case efsw::Actions::Delete:
            emit g_pInstance->sig_fileRemoved(QString::fromStdString(dir + filename));
            break;
        default:
            break;
        }
    }
};

DirectoryMonitor& DirectoryMonitor::instance()
{
    Q_ASSERT(g_pInstance);
    return *g_pInstance;
}

class DirectoryMonitor::PrivateData final
{
public:
    PrivateData() {}

    ~PrivateData() {}

    QString m_strDirPath;
    efsw::FileWatcher fileWatcher;
    FileWatchListener fileWatchListener;

private:
    Q_DISABLE_COPY(PrivateData)
};

DirectoryMonitor::DirectoryMonitor(const QString& strDirPath) : d(new PrivateData)
{
    d->m_strDirPath = strDirPath;

    Q_ASSERT(!g_pInstance);
    g_pInstance = this;

    d->fileWatcher.addWatch(strDirPath.toStdString(), &d->fileWatchListener, false);
    d->fileWatcher.watch();

    QDir().mkpath(strDirPath);
}

DirectoryMonitor::~DirectoryMonitor()
{
    Q_ASSERT(g_pInstance == this);
    g_pInstance = nullptr;
}

QVector<QString> DirectoryMonitor::getFilePaths(
    const QStringList& strlstNameFilters /* = QStringList()*/) const
{
    QVector<QString> strlstFilePaths;

    const QStringList strlstCurrentEntries =
        QDir(d->m_strDirPath)
            .entryList(strlstNameFilters, QDir::Files, QDir::Name | QDir::Reversed);

    strlstFilePaths.reserve(strlstCurrentEntries.size());

    for (const QString& strEntry : strlstCurrentEntries)
    {
        strlstFilePaths << Utils::cleanPath(d->m_strDirPath + "/" + strEntry);
    }

    return strlstFilePaths;
}
