#include "ClipboardCache.h"

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>

#include "ClipboardDataStream.h"
#include "ClipboardItem.h"
#include "Settings.h"

static ClipboardCache* g_pInstance = nullptr;

static constexpr quint8 c_nSerializerVersion{1};

static QString buildCacheFilePath()
{
    const QString strSettingsFilePath = SETTINGS.fileName();
    const QString strSettingsDirPath = QFileInfo(strSettingsFilePath).absolutePath();
    const QString strCacheFilePath = QDir::toNativeSeparators(strSettingsDirPath + "/cache.scb");
    return strCacheFilePath;
}

ClipboardCache& ClipboardCache::instance()
{
    Q_ASSERT(g_pInstance);
    return *g_pInstance;
}

ClipboardCache::ClipboardCache()
{
    Q_ASSERT(!g_pInstance);
    g_pInstance = this;

    // Initialize cache from the cache file.
    const QString strCacheFilePath = buildCacheFilePath();
    QFile file(strCacheFilePath);
    if (file.open(QIODevice::ReadOnly))
    {
        ClipboardDataStream stream(&file);

        quint8 nVersion{0};
        stream >> nVersion;
        if (nVersion == c_nSerializerVersion)
        {
            int nCount{0};
            stream >> nCount;
            for (int i = 0; i < nCount; ++i)
            {
                QString strFileName;
                stream >> strFileName;

                CachedItem cachedItem;
                stream >> cachedItem.m_dtTime >> cachedItem.m_nId >> cachedItem.m_strTag >>
                    cachedItem.m_nSize >> cachedItem.m_strPreview;

                m_mItems.emplace(strFileName.toStdString(), cachedItem);
            }
        }
    }
}

ClipboardCache::~ClipboardCache()
{
    Q_ASSERT(g_pInstance == this);
    g_pInstance = nullptr;

    // Remove entries that have not been accessed during the session.
    for (auto it = m_mItems.begin(); it != m_mItems.end();)
    {
        if (it->second.m_bAccessed)
        {
            ++it;
        }
        else
        {
            it = m_mItems.erase(it);
        }
    }

    // Serialize map to cache file.
    const QString strCacheFilePath = buildCacheFilePath();
    QFile file(strCacheFilePath);
    if (file.open(QIODevice::WriteOnly))
    {
        ClipboardDataStream stream(&file);

        stream << c_nSerializerVersion;

        const int nCount{static_cast<int>(m_mItems.size())};
        stream << nCount;
        for (const auto& entry : m_mItems)
        {
            const QString strFileName{QString::fromStdString(entry.first)};
            stream << strFileName;

            const CachedItem& cachedItem = entry.second;
            stream << cachedItem.m_dtTime << cachedItem.m_nId << cachedItem.m_strTag
                   << cachedItem.m_nSize << cachedItem.m_strPreview;
        }
    }
}

void ClipboardCache::purgeCache()
{
    m_mItems.clear();

    QFile(buildCacheFilePath()).remove();
}

bool ClipboardCache::loadFromCache(const QString& strFilePath, ClipboardItem& item)
{
    const QString strFileName{QFileInfo(strFilePath).fileName()};

    const auto it = m_mItems.find(strFileName.toStdString());
    if (it != m_mItems.end())
    {
        CachedItem& cachedItem = it->second;

        item.m_dtTime = cachedItem.m_dtTime;
        item.m_nId = cachedItem.m_nId;
        item.m_strTag = cachedItem.m_strTag;
        item.m_nSize = cachedItem.m_nSize;
        item.m_strPreview = cachedItem.m_strPreview;

        cachedItem.m_bAccessed = true;

        return true;
    }

    return false;
}

void ClipboardCache::saveToCache(const QString& strFilePath, const ClipboardItem& item)
{
    const QString strFileName{QFileInfo(strFilePath).fileName()};

    CachedItem& cachedItem = m_mItems[strFileName.toStdString()];

    cachedItem.m_dtTime = item.m_dtTime;
    cachedItem.m_nId = item.m_nId;
    cachedItem.m_strTag = item.m_strTag;
    cachedItem.m_nSize = item.m_nSize;
    cachedItem.m_strPreview = item.m_strPreview;

    cachedItem.m_bAccessed = true;
}

void ClipboardCache::removeFromCache(const QString& strFilePath)
{
    const QString strFileName{QFileInfo(strFilePath).fileName()};

    m_mItems.erase(strFileName.toStdString());
}
