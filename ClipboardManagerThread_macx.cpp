// clang-format off
#include "ClipboardManagerThread.h"
// clang-format on

// TODO: This is just the placeholder for the Mac OS implementation.

class ClipboardManagerThread::PrivateData final
{
public:
    PrivateData() {}

    ~PrivateData() {}

private:
    Q_DISABLE_COPY(PrivateData)
};

ClipboardManagerThread::ClipboardManagerThread() : d(new PrivateData)
{
    moveToThread(this);

    start();
}

ClipboardManagerThread::~ClipboardManagerThread()
{
    quit();
    wait();
}

bool ClipboardManagerThread::event(QEvent* pEvent)
{
    return QThread::event(pEvent);
}

void ClipboardManagerThread::run()
{
    exec();
}
