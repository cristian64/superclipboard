#ifndef SINGLEAPPLICATION_H
#define SINGLEAPPLICATION_H

#include <memory>

#include <QtCore/QLockFile>

class SingleApplication final
{
public:
    SingleApplication(const QString& strFilePath);
    ~SingleApplication();
    bool isOwner() const;

    Q_DISABLE_COPY(SingleApplication)

private:
    std::unique_ptr<QLockFile> m_apFile;
};

#endif  // SINGLEAPPLICATION_H
