#include "ClipboardManager.h"

#include <regex>

#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QProcess>
#include <QtCore/QSemaphore>
#include <QtCore/QTimer>
#include <QtGui/QClipboard>
#include <QtWidgets/QApplication>

#include "ClipboardItem.h"
#include "ClipboardManagerThread.h"
#include "Globals.h"
#include "MainWindow.h"
#include "QMimeDataEx.h"
#include "Settings.h"
#include "Utils.h"

static ClipboardManager* g_pInstance = nullptr;

namespace
{
QString processPatternSubstitutions(const QString& strText)
{
    std::string strProcessedText{strText.toStdString()};

    const QStringList strlstPatternSubstitutions{SETTING("pattern_substitutions").toStringList()};
    for (qsizetype i{0}; i < strlstPatternSubstitutions.size() / 2 * 2; i += 2)
    {
        const QString& strPattern{strlstPatternSubstitutions[i]};
        const QString& strSubstitution{strlstPatternSubstitutions[i + 1]};

        try
        {
            const std::regex regex(strPattern.toStdString());
            strProcessedText =
                std::regex_replace(strProcessedText, regex, strSubstitution.toStdString());
        }
        catch (const std::regex_error& e)
        {
            qWarning() << QString("Invalid pattern substitution (#%1): %2").arg(i).arg(e.what());
        }
    }

    return QString::fromStdString(strProcessedText);
}

QString processMarkdownToHtml(const QString& strText)
{
    std::string strProcessedText(strText.toStdString());

    {
        static const std::regex regex("\\[([^\\[\\]\\(\\)]+)\\]\\(([^\\[\\]\\(\\)]+)\\)");
        strProcessedText = std::regex_replace(strProcessedText, regex, "<a href=\"$2\">$1</a>");
    }

    {
        static const std::regex regex("\\*\\*([^\\*]+)\\*\\*");
        strProcessedText = std::regex_replace(strProcessedText, regex, "<b>$1</b>");
    }

    {
        static const std::regex regex("`([^`]+)`");
        strProcessedText = std::regex_replace(strProcessedText, regex, "<code>$1</code>");
    }

    {
        static const std::regex regex("_([^_]+)_");
        strProcessedText = std::regex_replace(strProcessedText, regex, "<i>$1</i>");
    }

    {
        static const std::regex regex("~~([^~]+)~~");
        strProcessedText = std::regex_replace(strProcessedText, regex, "<s>$1</s>");
    }

    return QString::fromStdString(strProcessedText);
}
}  // namespace

ClipboardManager& ClipboardManager::instance()
{
    Q_ASSERT(g_pInstance);
    return *g_pInstance;
}

class ClipboardManager::PrivateData final
{
public:
    PrivateData()
        : m_strAppFilePath(QApplication::applicationFilePath()),
          m_strDataPath(Globals::getDataDirPath()),
          m_bEnabled(true),
          m_pClipboard(QApplication::clipboard()),
          m_nIgnoreCount(0)
    {
        Q_ASSERT(m_pClipboard);
    }

    ~PrivateData() { m_pClipboard = nullptr; }
    const QString m_strAppFilePath;
    const QString m_strDataPath;
    bool m_bEnabled;
    QClipboard* m_pClipboard;
    ClipboardManagerThread m_thread;
    int m_nIgnoreCount;

private:
    Q_DISABLE_COPY(PrivateData)
};

ClipboardManager::ClipboardManager() : d(new PrivateData())
{
    Q_ASSERT(!g_pInstance);
    g_pInstance = this;

    if (d->m_pClipboard)
    {
        Q_CONNECT(d->m_pClipboard,
                  SIGNAL(changed(QClipboard::Mode)),
                  this,
                  SLOT(slt_clipboardChanged(QClipboard::Mode)));
        Q_CONNECT(&d->m_thread,
                  SIGNAL(sig_slotActivated(const quint8, const qint32)),
                  this,
                  SLOT(slt_slotActivated(const quint8, const qint32)));
        Q_CONNECT(&d->m_thread, SIGNAL(sig_toggleClipboard()), this, SIGNAL(sig_toggleClipboard()));
        Q_CONNECT(&d->m_thread, SIGNAL(sig_scrollUp()), this, SIGNAL(sig_scrollUp()));
        Q_CONNECT(&d->m_thread, SIGNAL(sig_scrollDown()), this, SIGNAL(sig_scrollDown()));
    }
}

ClipboardManager::~ClipboardManager()
{
    Q_ASSERT(g_pInstance == this);
    g_pInstance = nullptr;

    d.reset();
}

bool ClipboardManager::isEnabled() const
{
    return d->m_bEnabled;
}

void ClipboardManager::setEnabled(const bool bEnabled)
{
    d->m_bEnabled = bEnabled;
}

void ClipboardManager::setMimeData(const QMimeData& mimeData)
{
    ++d->m_nIgnoreCount;

    // If everything is okay, change the OS clipboard here.
    if (!d->m_pClipboard)
    {
        return;
    }
    d->m_pClipboard->setMimeData(QMimeDataEx::cloneMimeData(mimeData));
}

void ClipboardManager::emitSlotChanged(const quint8 nSlot)
{
    slt_slotActivated(nSlot, ClipboardManagerThread::ACTIVATION_MODE_STORE);
}

/*static */ void ClipboardManager::readAndStoreClipboardContent(
    const QString& strFilePath,
    const QDateTime& dtTime /* = QDateTime::currentDateTimeUtc()*/)
{
    const QClipboard* const pClipboard = QApplication::clipboard();
    if (!pClipboard)
    {
        return;
    }

    bool bHasData = false;
    QMimeData mimeData;

    for (int i = 0; i < 10; ++i)
    {
        mimeData.clear();

        // Make sure the data is fine.
        const QMimeData* const pMimeData = pClipboard->mimeData();
        if (!pMimeData)
        {
            continue;
        }

        // Make a copy already before emitting the signal.
        QMimeDataEx::copyMimeData(*pMimeData, mimeData);
        bHasData = QMimeDataEx::hasData(mimeData);

        // Make sure there is data available.
        if (bHasData)
        {
            break;
        }

        // Otherwise retry in a few ms. This is to work around QTBUG-27097
        // (See https://bugreports.qt.io/browse/QTBUG-27097 for more info).
        // Should affect only Windows, but I don't think there is any harm
        // to retry a few times also on Linux or Mac OS, which should
        // never happen anyway.
        QSemaphore semaphore;
        semaphore.tryAcquire(1, 100);
    }

    // No data after all? Ignore then.
    if (!bHasData)
    {
        return;
    }

    const ClipboardItem item(mimeData, dtTime);
    item.saveToFile(strFilePath, false);

    // Process pattern substitutions.
    if (mimeData.hasText() && strFilePath.endsWith(".history.scb"))
    {
        const QString strText{mimeData.text()};
        const QString strProcessedText{processPatternSubstitutions(strText)};
        const QString strProcessedHtml{processMarkdownToHtml(strProcessedText)};

        const bool bProcessedText{(strText != strProcessedText && !strProcessedText.isEmpty())};
        const bool bProcessedHtml{
            (strProcessedText != strProcessedHtml && !strProcessedHtml.isEmpty())};

        if (bProcessedText || bProcessedHtml)
        {
            QMimeData mimeDataAlt;
            if (!strProcessedText.isEmpty())
            {
                mimeDataAlt.setText(strProcessedText);
            }
            if (!strProcessedHtml.isEmpty())
            {
                mimeDataAlt.setHtml(strProcessedHtml);
            }

            const QDateTime dtTimeAlt{dtTime.addMSecs(1)};

            const QString strFilePathAlt{
                QFileInfo{QFileInfo{strFilePath}.dir(),
                          dtTimeAlt.toString("yyyyMMddhhmmsszzz") + ".history.scb"}
                    .filePath()};

            ClipboardItem item(mimeDataAlt, dtTimeAlt);
            item.m_strTag += "*";
            item.saveToFile(strFilePathAlt, false);
        }
    }
}

void ClipboardManager::slt_clipboardChanged(QClipboard::Mode mode)
{
    if (mode != QClipboard::Clipboard)
    {
        return;
    }

    // This is to prevent detecting ourselves.
    // If the value is larger than 0, we have to skip this one.
    if (d->m_nIgnoreCount)
    {
        --d->m_nIgnoreCount;
        return;
    }

    if (!d->m_bEnabled)
    {
        MainWindow::instance().blinkDisabledSystemTrayIcon();
        return;
    }

    // Run application in command line mode to write the current content to a
    // file.
    const QDateTime dtTime = QDateTime::currentDateTimeUtc();
    const QString strPrefix = dtTime.toString("yyyyMMddhhmmsszzz");
    const QString strFilePath = d->m_strDataPath + strPrefix + ".history.scb";
    const QString& strProgramPath = d->m_strAppFilePath;
    const QString strTime(QString::number(dtTime.toMSecsSinceEpoch()));
    const QStringList strlstArguments = QStringList() << strFilePath << strTime;
    QProcess::startDetached(strProgramPath, strlstArguments);
}

void ClipboardManager::slt_slotActivated(const quint8 nSlot, const qint32 eActivationMode)
{
    if (eActivationMode == ClipboardManagerThread::ACTIVATION_MODE_STORE)
    {
        const QString& strProgramPath = d->m_strAppFilePath;
        const QDateTime dtTime = QDateTime::currentDateTimeUtc();
        const QString strTime(QString::number(dtTime.toMSecsSinceEpoch()));

        // Run application in command line mode to write the current content
        // to a file.
        {
            const QString strPrefix = dtTime.toString("yyyyMMddhhmmsszzz");
            const QString strFilePath = d->m_strDataPath + strPrefix + ".slot_history.scb";
            const QStringList strlstArguments = QStringList() << strFilePath << strTime;
            QProcess::startDetached(strProgramPath, strlstArguments);
        }
        {
            const QString strFilePath =
                d->m_strDataPath + "slot" + QString::number(nSlot) + ".slot.scb";
            const QStringList strlstArguments = QStringList() << strFilePath << strTime;
            QProcess::startDetached(strProgramPath, strlstArguments);
        }
    }
    else
    {
        emit sig_slotRequested(nSlot);
    }
}
