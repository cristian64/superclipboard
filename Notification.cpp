#include "Notification.h"

#ifdef WIN32
#include <windows.h>
#endif

#include <QtCore/QPointer>
#include <QtCore/QTimer>
#include <QtGui/QPalette>
#include <QtGui/QScreen>
#include <QtWidgets/QApplication>

#include "Utils.h"
#include "ui_Notification.h"

static QPointer<Notification> g_pNotification;
static QPointer<QTimer> g_pTimer;

Notification::Notification()
    : QWidget(nullptr, Qt::WindowFlags() | Qt::ToolTip), ui(new Ui::Notification)
{
    ui->setupUi(this);

    // Set a white background for the notification.
    QPalette paletteCopy = palette();
    paletteCopy.setColor(QPalette::Window, Qt::white);
    setAutoFillBackground(true);
    setPalette(paletteCopy);

    // And a little border, in case the compositing window manager does not
    // provide the window with a drop shadow.
    setStyleSheet("QWidget#Notification { border: 1px solid #bbb; }");
}

Notification::~Notification()
{
    delete ui;
    ui = nullptr;
}

void Notification::showMessage(const QString& strTitle,
                               const QString& strMessage,
                               const int nTimeoutMs)
{
    if (!g_pTimer)
    {
        g_pTimer = new QTimer;
        g_pTimer->setSingleShot(true);
    }

    if (!g_pNotification)
    {
        g_pNotification = new Notification;
        Q_CONNECT(g_pTimer, SIGNAL(timeout()), g_pNotification, SLOT(hide()));

#ifdef WIN32
        // Frameless windows also lose their drop shadow. Qt does not seem to
        // provide a way for bringing it back; Windows API will need to be used.
        SetClassLongA(reinterpret_cast<HWND>(g_pNotification->winId()), GCL_STYLE, CS_DROPSHADOW);
#endif
    }

    // Create a dummy notification, for the sole purpose of calculating the
    // notification's size and position, before even showing it.
    Notification notification;
    notification.updateText(strTitle, strMessage);
    notification.autoAdjustSize();

    // Update the global notification with the known, final size, and show it.
    g_pNotification->updateText(strTitle, strMessage);
    g_pNotification->setFixedWidth(notification.minimumWidth());
    g_pNotification->setFixedHeight(notification.minimumHeight());
    g_pNotification->setGeometry(notification.geometry());
    g_pNotification->show();
    g_pNotification->raise();

    g_pTimer->start(nTimeoutMs);
}

void Notification::mousePressEvent(QMouseEvent* const event)
{
    QWidget::mousePressEvent(event);

    hide();
}

void Notification::updateText(const QString& strTitle, const QString& strMessage)
{
    setWindowTitle(strTitle);
    ui->labelTitle->setText(strTitle);
    ui->labelMessage->setText(strMessage);
}

void Notification::autoAdjustSize()
{
    // Calculate actual size of the widget. The following trick allows us to
    // force the layout without actually displaying it.
    show();
    layout()->invalidate();
    const QRect rGeometry = geometry();
    hide();

    // Get the geometry of the main screen, where the notification will be
    // displayed.
    const QScreen* screen = QApplication::primaryScreen();
#ifdef WIN32
    const QRect rScreenGeometry = screen->availableGeometry();
#else
    const QRect rScreenGeometry = screen->geometry();
#endif

    // Set size limits.
    constexpr double c_dWidthPercentage = 0.25;
    constexpr double c_dHeightPercentage = 0.75;
    const int nWidth =
        static_cast<int>(static_cast<double>(rScreenGeometry.width()) * c_dWidthPercentage);
    const int nMaxHeight =
        static_cast<int>(static_cast<double>(rScreenGeometry.height()) * c_dHeightPercentage);
    const int nHeight = nMaxHeight < rGeometry.height() ? nMaxHeight : rGeometry.height();
    setFixedWidth(nWidth);
    setFixedHeight(nHeight);

    // Move notification to its rigth place.
    constexpr int c_nMarginX = 10;
#ifdef WIN32
    constexpr int c_nMarginY = 10;
#else
    constexpr int c_nMarginY = 40;
#endif
#if 1  // At the bottom/right.
    const int nPosX = rScreenGeometry.x() + rScreenGeometry.width() - nWidth - c_nMarginX;
    const int nPosY = rScreenGeometry.y() + rScreenGeometry.height() - nHeight - c_nMarginY;
#else  // At the top/right.
    const int nPosX = rScreenGeometry.x() + rScreenGeometry.width() - nWidth - c_nMarginX;
    const int nPosY = rScreenGeometry.y() + c_nMarginY;
#endif
    move(nPosX, nPosY);
}
