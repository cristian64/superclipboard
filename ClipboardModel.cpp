#include "ClipboardModel.h"

#include <memory>
#include <vector>

#include <QtCore/QDateTime>
#include <QtCore/QDir>
#include <QtCore/QFileInfo>
#include <QtCore/QTimeZone>
#include <QtCore/QTimer>
#include <QtCore/QUrl>
#include <QtGui/QDesktopServices>
#include <QtWidgets/QApplication>
#include <QtWidgets/QInputDialog>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QPushButton>

#include "ClipboardCache.h"
#include "ClipboardItem.h"
#include "ClipboardManager.h"
#include "DirectoryMonitor.h"
#include "Globals.h"
#include "MainWindow.h"
#include "Settings.h"
#include "Utils.h"

#define SLOT_COUNT 9

class ClipboardModel::PrivateData final
{
public:
    PrivateData()
        : m_eClipboardModelType(CLIPBOARD_INVALID),
          m_strDataPath(Globals::getDataDirPath()),
          m_bClipboardToggled(false),
          m_nScrollIndex(0),
          m_nLastScrollTime(0)
    {
    }

    ~PrivateData()
    {
        for (ClipboardItem* const pItem : m_vItems)
        {
            if (!pItem)
            {
                continue;
            }
            delete pItem;
        }
        m_vItems.clear();
    }

    std::vector<ClipboardItem*> m_vItems;
    ClipboardModelType m_eClipboardModelType;
    const QString m_strDataPath;
    QString m_strFilter;
    bool m_bClipboardToggled;
    int m_nScrollIndex;
    qint64 m_nLastScrollTime;

private:
    Q_DISABLE_COPY(PrivateData)
};

ClipboardModel::ClipboardModel(const ClipboardModelType eClipboardModelType,
                               QObject* pParent /* = nullptr*/)
    : QAbstractItemModel(pParent), d(new PrivateData)
{
    d->m_eClipboardModelType = eClipboardModelType;

    // Connect to all the requierd signals in the constructor.
    DirectoryMonitor& directoryMonitor = DirectoryMonitor::instance();
    Q_CONNECT(&directoryMonitor,
              SIGNAL(sig_fileRemoved(const QString&)),
              this,
              SLOT(slt_fileRemoved(const QString&)));
    Q_CONNECT(&directoryMonitor,
              SIGNAL(sig_fileAdded(const QString&)),
              this,
              SLOT(slt_fileAdded(const QString&)));

    ClipboardManager& clipboardManager = ClipboardManager::instance();
    switch (eClipboardModelType)
    {
    case CLIPBOARD_HISTORY:
        Q_CONNECT(
            &clipboardManager, SIGNAL(sig_toggleClipboard()), this, SLOT(slt_toggleClipboard()));
        Q_CONNECT(&clipboardManager, SIGNAL(sig_scrollUp()), this, SLOT(slt_scrollUp()));
        Q_CONNECT(&clipboardManager, SIGNAL(sig_scrollDown()), this, SLOT(slt_scrollDown()));
        break;
    case CLIPBOARD_SLOT_HISTORY:
        break;
    case CLIPBOARD_SLOTS:
        Q_CONNECT(&clipboardManager,
                  SIGNAL(sig_slotRequested(const quint8)),
                  this,
                  SLOT(slt_slotRequested(const quint8)));
        d->m_vItems.resize(SLOT_COUNT);
        break;
    default:
        Q_ASSERT(0);
    }

    // Just going to insert all the files in the table.
    const QStringList strlstFilePaths =
        directoryMonitor.getFilePaths(QStringList("*" + getFileExtension()));
    switch (eClipboardModelType)
    {
    case CLIPBOARD_HISTORY:
    case CLIPBOARD_SLOT_HISTORY:
    {
        d->m_vItems.reserve(static_cast<size_t>(strlstFilePaths.count()));
        for (const QString& strFilePath : strlstFilePaths)
        {
            std::unique_ptr<ClipboardItem> apClipboardItem(new ClipboardItem());
            if (apClipboardItem->loadFromFile(strFilePath, false))
            {
                d->m_vItems.push_back(apClipboardItem.release());
            }
        }
        break;
    }
    case CLIPBOARD_SLOTS:
    {
        for (const QString& strFilePath : strlstFilePaths)
        {
            std::unique_ptr<ClipboardItem> apClipboardItem(new ClipboardItem());
            if (apClipboardItem->loadFromFile(strFilePath, false))
            {
                // Valid file name?
                const qint32 nSlotIndex = getSlotIndexByFilePath(strFilePath);
                if (nSlotIndex < 0 || nSlotIndex >= SLOT_COUNT)
                {
                    break;
                }

                d->m_vItems[static_cast<size_t>(nSlotIndex)] = apClipboardItem.release();
            }
        }
        break;
    }
    default:
        Q_ASSERT(0);
    }
}

ClipboardModel::~ClipboardModel()
{
    d.reset();
}

QModelIndex ClipboardModel::index(int row, int column, const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return createIndex(row, column);
}

QModelIndex ClipboardModel::parent(const QModelIndex& child) const
{
    Q_UNUSED(child);
    return QModelIndex();
}

int ClipboardModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return static_cast<int>(d->m_vItems.size());
}

int ClipboardModel::columnCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return COLUMN_COUNT;
}

QVariant ClipboardModel::headerData(int section,
                                    Qt::Orientation orientation,
                                    int role /* = Qt::DisplayRole*/) const
{
    if (orientation == Qt::Horizontal)
    {
        if (role == Qt::DisplayRole)
        {
            switch (section)
            {
            case COLUMN_TIME:
                return tr("Time");
            case COLUMN_TAG:
                return tr("Tag");
            case COLUMN_SIZE:
                return tr("Size");
            case COLUMN_PREVIEW:
                return tr("Preview");
            }
        }
    }
    else if (d->m_eClipboardModelType != CLIPBOARD_SLOTS && role == Qt::DisplayRole)
    {
        return static_cast<int>(d->m_vItems.size()) - section;
    }

    return QAbstractItemModel::headerData(section, orientation, role);
}

QHeaderView::ResizeMode ClipboardModel::getResizeMode(int section,
                                                      Qt::Orientation orientation) const
{
    if (orientation == Qt::Horizontal)
    {
        switch (section)
        {
        case COLUMN_TIME:
        case COLUMN_SIZE:
            return QHeaderView::ResizeToContents;
        case COLUMN_PREVIEW:
            return QHeaderView::Stretch;
        }
    }

    return QHeaderView::Interactive;
}

QVariant ClipboardModel::data(const QModelIndex& index, int role) const
{
    switch (role)
    {
    case Qt::DisplayRole:
    {
        if (index.row() < 0 || static_cast<int>(d->m_vItems.size()) <= index.row())
        {
            break;
        }
        const ClipboardItem* const pItem = d->m_vItems[static_cast<size_t>(index.row())];
        if (!pItem)
        {
            break;
        }

        switch (index.column())
        {
        case COLUMN_TIME:
            return pItem->m_dtTime.toLocalTime().toString("yyyy-MM-dd hh:mm:ss.zzz");
        case COLUMN_TAG:
            return pItem->m_strTag;
        case COLUMN_SIZE:
            return Utils::convertToHumanReadableSize(pItem->m_nSize);
        case COLUMN_PREVIEW:
            return pItem->m_strPreview;
        }

        Q_ASSERT_X(0, Q_FUNC_INFO, "Column not defined");
        return tr("Invalid column");
    }
    case Qt::TextAlignmentRole:
    {
        return Qt::AlignVCenter;
    }
    case Qt::BackgroundRole:
    case Qt::ForegroundRole:
    {
        if (index.row() < 0 || static_cast<int>(d->m_vItems.size()) <= index.row())
        {
            break;
        }
        const ClipboardItem* const pItem = d->m_vItems[static_cast<size_t>(index.row())];
        if (!pItem)
        {
            break;
        }

        if (!pItem->needsHighlighting(d->m_strFilter))
        {
            break;
        }

        return role == Qt::BackgroundRole ? QBrush(QColor(255, 255, 170)) : QBrush(Qt::black);
    }
    }

    return QVariant();
}

void ClipboardModel::showEditTagDialog(const quint32 nIndex)
{
    // Show dialog with the current value and let the user write a new tag.
    const ClipboardItem* const pItem = getClipboardItem(nIndex);
    if (!pItem)
    {
        return;
    }

    QInputDialog inputDialog(&MainWindow::instance());
    inputDialog.setOkButtonText(tr("&Save"));
    inputDialog.setCancelButtonText(tr("&Cancel"));
    inputDialog.setWindowTitle(tr("New tag"));
    inputDialog.setLabelText(tr("Enter a new tag for the clipboard item."));
    inputDialog.setTextValue(pItem->m_strTag);
    if (inputDialog.exec())
    {
        // If dialog accepted, load item and store it again.
        const QString strNewTag = inputDialog.textValue();
        const ClipboardItem newItem(pItem->m_mimeData, pItem->m_dtTime, strNewTag);
        const quint8 nSlot = static_cast<quint8>(nIndex + 1);
        newItem.saveToFile(cookFilePath(&newItem, nSlot), true);
    }
}

void ClipboardModel::showCustomContextMenu(const QList<quint32>& nlstIndexes)
{
    if (nlstIndexes.isEmpty())
    {
        return;
    }

    // See if the selection is actually containing anything relevant.
    const ClipboardItem* pBrowseItem = nullptr;
    QList<const ClipboardItem*> lstItems;
    qint32 nFirstItemIndex = -1;
    for (const quint32 nIndex : nlstIndexes)
    {
        if (nIndex >= quint32(d->m_vItems.size()))
        {
            continue;
        }
        const ClipboardItem* pItem = d->m_vItems[nIndex];
        if (!pItem)
        {
            continue;
        }

        lstItems << pItem;
        if (nFirstItemIndex < 0)
        {
            nFirstItemIndex = static_cast<qint32>(nIndex);
        }

        if (!pBrowseItem && Utils::isValidUrl(pItem->m_strPreview))
        {
            pBrowseItem = pItem;
        }
    }

    const bool bShowRetrieve = lstItems.size() && nlstIndexes.count() == 1;
    const bool bShowStore = nlstIndexes.count() == 1 && d->m_eClipboardModelType == CLIPBOARD_SLOTS;
    const bool bShowBrowse = pBrowseItem != nullptr && nlstIndexes.count() == 1;
    const bool bShowEdit = nFirstItemIndex >= 0;
    const bool bShowDelete = lstItems.size();

    // Show the context menu.
    QMenu menu;
    QAction* pActionRetrieve = menu.addAction(tr("Retrieve"));
    pActionRetrieve->setIcon(QIcon(":/resources/retrieve.svg"));
    pActionRetrieve->setEnabled(bShowRetrieve);
    QAction* pActionStore = menu.addAction(tr("Store"));
    pActionStore->setIcon(QIcon(":/resources/store.svg"));
    pActionStore->setEnabled(bShowStore);
    pActionStore->setVisible(d->m_eClipboardModelType == CLIPBOARD_SLOTS);
    QAction* pActionBrowse = menu.addAction(tr("Browse"));
    pActionBrowse->setIcon(QIcon(":/resources/browse.svg"));
    pActionBrowse->setEnabled(bShowBrowse);
    QAction* pActionEdit = menu.addAction(tr("Edit tag"));
    pActionEdit->setIcon(QIcon(":/resources/edit.svg"));
    pActionEdit->setEnabled(bShowEdit);
    QAction* pActionDelete = menu.addAction(tr("Delete"));
    pActionDelete->setIcon(QIcon(":/resources/delete.svg"));
    pActionDelete->setEnabled(bShowDelete);
    menu.show();
    const QAction* pAction = menu.exec(QCursor::pos());
    if (!pAction)
    {
        return;
    }

    // Depending on the action clicked, do this or that.
    if (pAction == pActionRetrieve)
    {
        slt_clipboardRequested(nlstIndexes.first());
    }
    else if (pAction == pActionStore)
    {
        ClipboardManager::instance().emitSlotChanged(static_cast<quint8>(nlstIndexes.first() + 1));
    }
    else if (pAction == pActionBrowse)
    {
        Q_ASSERT(pBrowseItem);
        if (pBrowseItem)
        {
            QDesktopServices::openUrl(QUrl(pBrowseItem->m_strPreview));
        }
    }
    else if (pAction == pActionEdit)
    {
        showEditTagDialog(static_cast<quint32>(nFirstItemIndex));
    }
    else if (pAction == pActionDelete)
    {
        slt_clipboardDelete(nlstIndexes);
    }
}

const ClipboardItem* ClipboardModel::getClipboardItem(const quint32 nIndex,
                                                      const bool bFullLoad) const
{
    if (nIndex >= quint32(d->m_vItems.size()))
    {
        return nullptr;
    }

    ClipboardItem* const pItem = d->m_vItems[nIndex];
    if (!pItem)
    {
        return nullptr;
    }

    // If it isn't fully loaded, we need to read from file now.
    if (bFullLoad && !pItem->m_bFullLoad)
    {
        const QString strFilePath =
            cookFilePath(pItem, static_cast<quint8>(nIndex + 1) /*only used if "slot" mode*/);
        const bool bLoaded = pItem->loadFromFile(strFilePath, true);
        if (!bLoaded)
        {
            return nullptr;
        }
    }

    return pItem;
}

const QString& ClipboardModel::getFilter() const
{
    return d->m_strFilter;
}

void ClipboardModel::slt_filterChanged(const QString& strFilter)
{
    if (d->m_strFilter == strFilter)
    {
        return;
    }

    beginResetModel();
    d->m_strFilter = strFilter;
    endResetModel();
}

void ClipboardModel::slt_slotRequested(const quint8 nSlot)
{
    // Is the slot valid?
    if (!nSlot)
    {
        return;
    }
    const quint8 nSlotIndex = static_cast<quint8>(nSlot - 1);
    slt_clipboardRequested(nSlotIndex);
}

void ClipboardModel::slt_clipboardRequested(const quint32 nIndex)
{
    const ClipboardItem* const pItem = getClipboardItem(nIndex);
    if (!pItem)
    {
        return;
    }

    ClipboardManager::instance().setMimeData(pItem->m_mimeData);

    const quint32 nNumber = d->m_eClipboardModelType == CLIPBOARD_SLOTS
                                ? nIndex + 1
                                : static_cast<quint32>(d->m_vItems.size()) - nIndex;
    emit sig_showMessage(
        tr("Slot %1 retrieved").arg(nNumber), pItem->m_strPreview, QSystemTrayIcon::NoIcon, 2000);
}

void ClipboardModel::slt_clipboardDelete(const QList<quint32>& nlstIndexes)
{
    // Prompt the user for confirmation.
    if (SETTING("Behaviour/confirm_deletion", true).toBool())
    {
        QMessageBox messageBox(QMessageBox::Icon::Question,
                               tr("Confirm deletion"),
                               tr("Delete selected entries?"),
                               QMessageBox::NoButton,
                               &MainWindow::instance());
        QPushButton deleteButton(tr("&Delete"), &messageBox);
        QPushButton cancelButton(tr("&Cancel"), &messageBox);
        messageBox.addButton(&deleteButton, QMessageBox::AcceptRole);
        messageBox.addButton(&cancelButton, QMessageBox::RejectRole);
        messageBox.setDefaultButton(&deleteButton);
        messageBox.setEscapeButton(&cancelButton);
        messageBox.exec();
        if (messageBox.clickedButton() != &deleteButton)
        {
            return;
        }
    }

    switch (d->m_eClipboardModelType)
    {
    case CLIPBOARD_HISTORY:
    case CLIPBOARD_SLOT_HISTORY:
    {
        for (const quint32 nIndex : nlstIndexes)
        {
            if (nIndex >= quint32(d->m_vItems.size()))
            {
                continue;
            }
            const ClipboardItem* const pItem = d->m_vItems[nIndex];
            if (!pItem)
            {
                continue;
            }

            QFile(cookFilePath(pItem, 0)).remove();
        }
        break;
    }
    case CLIPBOARD_SLOTS:
    {
        for (const quint32 nIndex : nlstIndexes)
        {
            QFile(cookFilePath(nullptr, static_cast<quint8>(nIndex + 1))).remove();
        }
        break;
    }
    default:
        Q_ASSERT(0);
    }
}

void ClipboardModel::slt_toggleClipboard()
{
    // Bail if there aren't at least two clipboards.
    if (d->m_vItems.size() < 2)
    {
        return;
    }

    // Find the index of the second unique clipboard (use preview for
    // comparison).
    quint32 nIndex = 0;
    const ClipboardItem* const pLastItem = d->m_vItems[0];
    for (const ClipboardItem* const pItem : d->m_vItems)
    {
        if (pItem->m_strPreview != pLastItem->m_strPreview)
        {
            break;
        }
        ++nIndex;
    }

    const ClipboardItem* const pItem = getClipboardItem(d->m_bClipboardToggled ? 0 : nIndex);
    if (!pItem)
    {
        return;
    }

    ClipboardManager::instance().setMimeData(pItem->m_mimeData);

    emit sig_showMessage(
        tr("Clipboard toggled"), pItem->m_strPreview, QSystemTrayIcon::NoIcon, 2000);

    d->m_bClipboardToggled = !d->m_bClipboardToggled;

    QTimer::singleShot(0, &MainWindow::instance(), SLOT(slt_systemTrayIconToggled()));
}

void ClipboardModel::slt_scrollUp()
{
    scroll(-1);
}

void ClipboardModel::slt_scrollDown()
{
    scroll(1);
}

void ClipboardModel::slt_fileRemoved(const QString& strFilePath)
{
    switch (d->m_eClipboardModelType)
    {
    case CLIPBOARD_HISTORY:
    case CLIPBOARD_SLOT_HISTORY:
    {
        // Find, delete and remove the item.
        const qint32 nIndex = findIndexByFilePath(strFilePath);
        if (nIndex >= 0)
        {
            beginRemoveRows(QModelIndex(), nIndex, nIndex);
            delete d->m_vItems[static_cast<size_t>(nIndex)];
            d->m_vItems.erase(d->m_vItems.begin() + nIndex);
            endRemoveRows();
        }
        break;
    }
    case CLIPBOARD_SLOTS:
    {
        // Valid file name?
        const qint32 nSlotIndex = getSlotIndexByFilePath(strFilePath);
        if (nSlotIndex < 0 || nSlotIndex >= SLOT_COUNT)
        {
            break;
        }

        // Is item set? Delete and remove it.
        const ClipboardItem* pItem = d->m_vItems[static_cast<size_t>(nSlotIndex)];
        if (pItem)
        {
            delete pItem;
            d->m_vItems[static_cast<size_t>(nSlotIndex)] = nullptr;
            dataChanged(createIndex(nSlotIndex, COLUMN_FIRST),
                        createIndex(nSlotIndex, COLUMN_COUNT - 1));
        }
        break;
    }
    default:
        Q_ASSERT(0);
    }

    // Reset scroll.
    d->m_nScrollIndex = 0;
}

void ClipboardModel::slt_fileAdded(const QString& strFilePath)
{
    // Only if the extension is the expected one.
    if (!strFilePath.endsWith(getFileExtension(), Qt::CaseInsensitive))
    {
        return;
    }

    // Evict entry in the cache.
    ClipboardCache::instance().removeFromCache(strFilePath);

    // Try to load first.
    std::unique_ptr<ClipboardItem> apClipboardItem(new ClipboardItem());
    if (!apClipboardItem->loadFromFile(strFilePath, false))
    {
        return;
    }

    switch (d->m_eClipboardModelType)
    {
    case CLIPBOARD_HISTORY:
    case CLIPBOARD_SLOT_HISTORY:
    {
        const qint32 nIndex = findIndexByFilePath(strFilePath);
        if (nIndex < 0)
        {
            // Insert if it isn't already present.
            beginInsertRows(QModelIndex(), 0, 0);
            d->m_vItems.insert(d->m_vItems.begin(), apClipboardItem.release());
            endInsertRows();

            // Remove the excess. Only if history.
            // For the slots, we want to keep it forever.
            if (d->m_eClipboardModelType == CLIPBOARD_HISTORY)
            {
                const quint32 nMaxHistoryCount = SETTING("max_history_count", 1000).toUInt();
                const quint32 nCurrentCount = static_cast<quint32>(d->m_vItems.size());
                if (nMaxHistoryCount < nCurrentCount)
                {
                    beginRemoveRows(QModelIndex(),
                                    static_cast<int>(nMaxHistoryCount),
                                    static_cast<int>(nCurrentCount - 1));
                    for (quint32 i = 0; i < nCurrentCount - nMaxHistoryCount; ++i)
                    {
                        std::unique_ptr<ClipboardItem> apItem(d->m_vItems[nCurrentCount - 1 - i]);
                        QFile(cookFilePath(apItem.get(), 0)).remove();
                    }
                    d->m_vItems.resize(nMaxHistoryCount);
                    endRemoveRows();
                }
            }

            // Reset the toggle state whenever history is written.
            if (d->m_eClipboardModelType == CLIPBOARD_HISTORY)
            {
                d->m_bClipboardToggled = false;
            }
        }
        else
        {
            // Delete previous item (if any).
            std::unique_ptr<ClipboardItem>(d->m_vItems[static_cast<size_t>(nIndex)]);

            // And set the new one.
            d->m_vItems[static_cast<size_t>(nIndex)] = apClipboardItem.release();
            dataChanged(createIndex(nIndex, COLUMN_FIRST), createIndex(nIndex, COLUMN_COUNT - 1));
        }

        // Excite the system tray icon.
        if (d->m_eClipboardModelType == CLIPBOARD_HISTORY)
        {
            QTimer::singleShot(0, &MainWindow::instance(), SLOT(slt_systemTrayIconExcited()));
        }
        break;
    }
    case CLIPBOARD_SLOTS:
    {
        // Valid file name?
        const qint32 nSlotIndex = getSlotIndexByFilePath(strFilePath);
        if (nSlotIndex < 0 || nSlotIndex >= SLOT_COUNT)
        {
            break;
        }

        // Delete previous item (if any).
        std::unique_ptr<ClipboardItem>(d->m_vItems[static_cast<size_t>(nSlotIndex)]);

        // Set new item.
        d->m_vItems[static_cast<size_t>(nSlotIndex)] = apClipboardItem.release();
        dataChanged(createIndex(nSlotIndex, COLUMN_FIRST),
                    createIndex(nSlotIndex, COLUMN_COUNT - 1));

        emit sig_showMessage(
            tr("Slot %1 updated").arg(nSlotIndex + 1), QString(), QSystemTrayIcon::NoIcon, 2000);

        break;
    }
    default:
        Q_ASSERT(0);
    }

    // Reset scroll.
    d->m_nScrollIndex = 0;
}

qint32 ClipboardModel::findIndexByFilePath(const QString& strFilePath) const
{
    const QString strFileBaseName = QFileInfo(strFilePath).baseName();
    const QDate dDate = QDate::fromString(strFileBaseName.left(8), "yyyyMMdd");
    const QTime tTime = QTime::fromString(strFileBaseName.right(9), "hhmmsszzz");
    const QDateTime dtTime = QDateTime(dDate, tTime, QTimeZone::utc());
    const qint64 nId = dtTime.toMSecsSinceEpoch();

    const quint32 nCount = static_cast<quint32>(d->m_vItems.size());
    for (quint32 i = 0; i < nCount; ++i)
    {
        const ClipboardItem* pItem = d->m_vItems[i];
        if (pItem && pItem->m_nId == nId)
        {
            return static_cast<qint32>(i);
        }
    }

    return -1;
}

qint32 ClipboardModel::getSlotIndexByFilePath(const QString& strFilePath) const
{
    const auto nExtensionLength = getFileExtension().length() + 1;
    const QString strSlot = strFilePath[strFilePath.length() - nExtensionLength];
    const qint32 nSlot = strSlot.toInt();
    const qint32 nSlotIndex = nSlot - 1;
    return nSlotIndex;
}

QString ClipboardModel::cookFilePath(const ClipboardItem* const pItem, const quint8 nSlot) const
{
    switch (d->m_eClipboardModelType)
    {
    case CLIPBOARD_HISTORY:
    case CLIPBOARD_SLOT_HISTORY:
    {
        if (!pItem)
        {
            Q_ASSERT(0);
            return QString();
        }
        const QString strDateTimeUtc = pItem->m_dtTime.toString("yyyyMMddhhmmsszzz");
        const QString strFilePath = d->m_strDataPath + strDateTimeUtc + getFileExtension();
        return strFilePath;
    }
    case CLIPBOARD_SLOTS:
    {
        if (!nSlot)
        {
            Q_ASSERT(0);
            return QString();
        }
        const QString strFilePath =
            d->m_strDataPath + "slot" + QString::number(nSlot) + getFileExtension();
        return strFilePath;
    }
    default:
        Q_ASSERT(0);
        return QString();
    }
}

QString ClipboardModel::getFileExtension() const
{
    switch (d->m_eClipboardModelType)
    {
    case CLIPBOARD_HISTORY:
        return ".history.scb";
    case CLIPBOARD_SLOT_HISTORY:
        return ".slot_history.scb";
    case CLIPBOARD_SLOTS:
        return ".slot.scb";
    default:
        Q_ASSERT(0);
        return QString();
    }
}

void ClipboardModel::scroll(int nDirection)
{
    // Reset scroll index if last scroll happened "long" ago.
    const qint64 nCurrentTime = QDateTime::currentSecsSinceEpoch();
    if (nCurrentTime - d->m_nLastScrollTime > 5)
    {
        d->m_nScrollIndex = 0;
    }
    d->m_nLastScrollTime = nCurrentTime;

    // Update and clamp scroll index.
    d->m_nScrollIndex =
        std::min(static_cast<int>(d->m_vItems.size()), std::max(0, d->m_nScrollIndex + nDirection));

    // Bail if there isn't at least one clipboard.
    if (d->m_vItems.empty())
    {
        return;
    }

    const ClipboardItem* const pItem = getClipboardItem(static_cast<quint32>(d->m_nScrollIndex));
    if (!pItem)
    {
        return;
    }

    ClipboardManager::instance().setMimeData(pItem->m_mimeData);

    const int nNumber = static_cast<int>(d->m_vItems.size()) - d->m_nScrollIndex;
    emit sig_showMessage(
        tr("Slot %1 retrieved").arg(nNumber), pItem->m_strPreview, QSystemTrayIcon::NoIcon, 2000);

    QTimer::singleShot(0, &MainWindow::instance(), SLOT(slt_systemTrayIconToggled()));
}

ClipboardSortFilterProxyModel::ClipboardSortFilterProxyModel(QObject* parent)
    : QSortFilterProxyModel(parent)
{
}

bool ClipboardSortFilterProxyModel::filterAcceptsRow(int row, const QModelIndex& parent) const
{
    Q_UNUSED(parent);

    const ClipboardModel* const pModel = dynamic_cast<ClipboardModel* const>(sourceModel());
    if (!pModel)
    {
        return true;
    }

    const QString& strFilter = pModel->getFilter();
    if (strFilter.isEmpty())
    {
        return true;
    }

    const ClipboardItem* const pItem = pModel->getClipboardItem(static_cast<quint32>(row), false);
    if (!pItem)
    {
        return true;
    }

    return pItem->needsHighlighting(strFilter);
}
